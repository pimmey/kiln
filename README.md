# Kiln

A web app for co-working company

## Installing
Clone project and install dependencies 
```
yarn
```
or
```
npm i
```

## Developing
Kiln is powered by [Parcel](http://parceljs.org) and you can start the development server with
```
yarn start
```

## Building
Compile, minify, etc. with
```
yarn build
```

Sometimes there's a need to make a build without minifying the code, can be achieved with the following:
```
yarn build:nomin
```


The code will be placed in `dist/`.

## Deploying
Deployment is done via rsync and is available for dev and live servers
```
yarn deploy:dev
yarn deploy:live
```

For a preview of the deployment process, the following commands have been added:
```
yarn predeploy:dev
yarn predeploy:live
```