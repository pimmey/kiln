import 'babel-polyfill'
import 'es6-promise/auto'
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { ThemeProvider } from 'emotion-theming'
import 'normalize.css/normalize.css'

import 'Styles/globalStyles'
import theme from 'Styles/theme'
import Router from './screens/router'
import store from './state/store'
import { getSettings } from './state/ducks/settings'

store.dispatch(getSettings())

render(
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <Router />
    </ThemeProvider>
  </Provider>,
  document.getElementById('app')
)
