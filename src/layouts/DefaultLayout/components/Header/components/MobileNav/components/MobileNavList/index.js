import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import styled from 'react-emotion'
import { NavLink } from 'react-router-dom'

import Config from 'Utils/config'
import { toggleMobileNav } from 'State/ducks/mobileNav'

const StyledNavList = styled.ul`
  margin: 18px 0 0 27px;
`

const StyledNavLink = styled(NavLink)`
   font-size: 40px;
   color: ${prop => prop.theme.colors.opaqueBlue};
   
   &.active {
     font-weight: bold;
     color: ${prop => prop.theme.colors.darkBlue};
   }
`

class MobileNavList extends PureComponent {
  handleClick = e => {
    this.props.dispatch(toggleMobileNav())
  }

  render () {
    return (
      <StyledNavList>
        {Config.pages.map(page => (
          <li key={page}>
            <StyledNavLink
              to={`/${page.toLowerCase()}`}
              onClick={this.handleClick}
            >
              {page}
            </StyledNavLink>
          </li>
        ))}
      </StyledNavList>
    )
  }
}

export default connect()(MobileNavList)
