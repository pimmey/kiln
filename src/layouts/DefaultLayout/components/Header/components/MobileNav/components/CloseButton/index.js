import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import styled from 'react-emotion'

import closeButtonSrc from 'Images/close.svg'
import { toggleMobileNav } from 'State/ducks/mobileNav'

const StyledCloseButton = styled.a`
  position: absolute;
  top: 30px;
  right: 30px;
`

class CloseButton extends PureComponent {
  handleClick = e => {
    e.preventDefault()
    this.props.dispatch(toggleMobileNav())
  }

  render () {
    return (
      <StyledCloseButton href='#!' onClick={this.handleClick}>
        <img src={closeButtonSrc} alt='Close' />
      </StyledCloseButton>
    )
  }
}

export default connect()(CloseButton)
