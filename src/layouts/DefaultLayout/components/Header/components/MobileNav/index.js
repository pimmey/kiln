import React, { PureComponent } from 'react'
import { bool } from 'prop-types'
import { connect } from 'react-redux'
import styled from 'react-emotion'

import CloseButton from './components/CloseButton'
import MobileNavList from './components/MobileNavList'
import NavLinks from './../NavLinks'
import { selectShowMobileNav } from 'State/ducks/mobileNav'

const StyledMobileNav = styled.nav`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 999;
  display: ${prop => prop.show ? 'block' : 'none'};
  width: 100%;
  height: 100%;
  background: ${prop => prop.theme.colors.coral};
`

const NavLinksWrapper = styled.div`
  position: absolute;
  right: 23px;
  bottom: 27px;
`

class MobileNav extends PureComponent {
  render () {
    return (
      <StyledMobileNav show={this.props.showMobileNav}>
        <CloseButton />
        <MobileNavList />
        <NavLinksWrapper>
          <NavLinks />
        </NavLinksWrapper>
      </StyledMobileNav>
    )
  }
}

MobileNav.propTypes = {
  showMobileNav: bool.isRequired
}

export default connect(state => ({
  showMobileNav: selectShowMobileNav(state)
}))(MobileNav)
