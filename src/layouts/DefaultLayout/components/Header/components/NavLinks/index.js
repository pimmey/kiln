import React from 'react'
import styled from 'react-emotion'

import ColoredText from 'Components/ColoredText'
import Item from '../Nav/components/ListItem'

const StyledLink = styled.a`
  font-size: 12px;
  text-transform: uppercase;
  color: ${prop => prop.theme.colors.opaqueBlue};
`

const Icon = styled.i`
  position: relative;
  top: 5px;
  font-size: 20px;
`

const NavLinks = () => (
  <ul>
    <Item>
      <StyledLink href='#!'>Log in</StyledLink>
    </Item>
    <Item>
      <StyledLink href='#!'>
        <ColoredText.darkBlue>
          <strong>
            Join Kiln
          </strong>
          <Icon className='material-icons'>keyboard_arrow_right</Icon>
        </ColoredText.darkBlue>
      </StyledLink>
    </Item>
  </ul>
)

export default NavLinks
