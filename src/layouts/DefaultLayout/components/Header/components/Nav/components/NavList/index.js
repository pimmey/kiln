import React from 'react'
import styled from 'react-emotion'
import { NavLink } from 'react-router-dom'

import Config from 'Utils/config'
import Item from './../ListItem'

const StyledNavList = styled.ul`
  margin: 0 0 0 40px;
`

const StyledNavLink = styled(NavLink)`
  font-size: 1.125rem;
  color: ${prop => prop.theme.colors.opaqueBlue};
  
  &.active {
    position: relative;
    font-weight: bold;
    color: ${prop => prop.theme.colors.darkBlue};
    
    &::before {
      position: absolute;
      top: -67px;
      display: block;
      width: 100%;
      height: 2px;
      background: ${prop => prop.theme.colors.darkBlue};
      content: '';
    }
  }
`

const NavList = () => (
  <StyledNavList>
    {Config.pages.map(page => (
      <Item key={page}>
        <StyledNavLink to={`/${page.toLowerCase()}`}>{page}</StyledNavLink>
      </Item>
    ))}
  </StyledNavList>
)

export default NavList
