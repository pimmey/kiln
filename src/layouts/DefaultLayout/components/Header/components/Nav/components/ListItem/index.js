import React from 'react'
import styled from 'react-emotion'

const StyledListItem = styled.li`
  display: inline-flex;
  
  &:not(:last-of-type) {
    margin-right: 20px;
  }
`

const ListItem = ({ children }) => <StyledListItem>{children}</StyledListItem>

export default ListItem
