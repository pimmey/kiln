import React from 'react'
import styled from 'react-emotion'

import NavList from './components/NavList'
import NavLinks from './../NavLinks'

const StyledNav = styled.nav`
  position: relative;
  top: -2px;
  display: flex;
  flex-grow: 1;
  justify-content: space-between;
  align-items: flex-end;
`

const Nav = () => (
  <StyledNav>
    <NavList />
    <NavLinks />
  </StyledNav>
)

export default Nav
