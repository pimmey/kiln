import React from 'react'
import styled from 'react-emotion'

import logoSrc from 'Images/logo.svg'

const Img = styled.img`
  width: 99px;
  
  @media only screen and (max-width: 767px) {
    width: 79px;
  }
`

const Logo = () => <Img src={logoSrc} alt='Kiln' />

export default Logo
