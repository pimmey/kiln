import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import styled from 'react-emotion'

import hamburgerSrc from './images/hamburger.svg'
import { toggleMobileNav } from 'State/ducks/mobileNav'

const Button = styled.a`
  position: relative;
  top: 4px;
`

class Hambruger extends PureComponent {
  handleClick = e => {
    e.preventDefault()
    this.props.dispatch(toggleMobileNav())
  }

  render () {
    return (
      <Button href='#!' onClick={this.handleClick}>
        <img src={hamburgerSrc} alt='Menu' />
      </Button>
    )
  }
}

export default connect()(Hambruger)
