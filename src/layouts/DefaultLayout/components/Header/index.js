import React, { PureComponent } from 'react'
import styled from 'react-emotion'
import { Grid, Row, Col } from 'react-flexbox-grid'
import Media from 'react-media'
import { NavLink } from 'react-router-dom'

import Logo from './components/Logo'
import Nav from './components/Nav'
import Hamburger from './components/Hamburger'
import MobileNav from './components/MobileNav'

const StyledHeader = styled.header`
  margin: 29px 0;
  
  @media only screen and (max-width: 767px) {
    margin: 10px 0;
  }
`

const FlexCol = styled(Col)`
  display: flex;
  
  @media only screen and (max-width: 767px) {
    align-items: center;
    justify-content: space-between;
  }
`

class Header extends PureComponent {
  render () {
    return (
      <StyledHeader>
        <Grid>
          <Row>
            <FlexCol xs={12}>
              <NavLink to='/' style={{ display: 'inline-flex' }}>
                <Logo />
              </NavLink>
              <Media
                query='(min-width: 768px)'
                render={() => <Nav />}
              />
              <Media
                query='(max-width: 767px)'
                render={() => (
                  <div>
                    <Hamburger />
                    <MobileNav />
                  </div>
                )}
              />
            </FlexCol>
          </Row>
        </Grid>
      </StyledHeader>
    )
  }
}

export default Header
