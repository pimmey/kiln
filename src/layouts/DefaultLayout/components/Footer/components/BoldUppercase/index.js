import React from 'react'
import styled from 'react-emotion'

const StyledBoldUppercase = styled.div`
  margin: 5px 0;
  font-weight: bold;
  text-transform: uppercase;
`

const BoldUppercase = ({ children }) => <StyledBoldUppercase>{children}</StyledBoldUppercase>

export default BoldUppercase
