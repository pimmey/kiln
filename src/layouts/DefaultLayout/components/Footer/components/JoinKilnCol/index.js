import React from 'react'

import Text from 'Components/ColoredText'
import FlexEndCol from './../FlexEndCol'
import BoldUppercase from './../BoldUppercase'
import JoinIcon from './components/JoinIcon'

const JoinKilnCol = () => (
  <FlexEndCol xs={12} md={3} className='join-kiln-col'>
    <JoinIcon />
    <BoldUppercase>
      <Text.coral>Join Kiln</Text.coral>
    </BoldUppercase>
    <div>
      Become a member of Utah’s
      <br />
      most vibrant tech community
    </div>
  </FlexEndCol>
)

export default JoinKilnCol
