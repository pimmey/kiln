import React from 'react'
import styled from 'react-emotion'

const IconWrapper = styled.div`
  position: relative;
  top: -8px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 32px;
  height: 32px;
  border: 1px solid ${prop => prop.theme.colors.opaqueWhite};
  border-radius: 50%;
`

const Icon = styled.i`
  font-size: 20px;
  color: ${prop => prop.theme.colors.coral};
`

const JoinIcon = () => (
  <IconWrapper>
    <Icon className='material-icons'>keyboard_arrow_right</Icon>
  </IconWrapper>
)

export default JoinIcon
