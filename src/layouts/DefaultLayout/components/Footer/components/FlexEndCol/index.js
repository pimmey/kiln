import React from 'react'
import styled from 'react-emotion'
import { Col } from 'react-flexbox-grid'

const StyledFlexEndCol = styled(Col)`
  display: flex;
  align-items: flex-start;
  flex-direction: column;
  justify-content: flex-end;
  
  &.join-kiln-col {
    @media only screen and (max-width: 768px) {
      margin-bottom: 20px
    }
  }
`

const FlexEndCol = ({ children, className, ...props }) => (
  <StyledFlexEndCol
    className={className}
    {...props}
  >
    {children}
  </StyledFlexEndCol>
)

export default FlexEndCol
