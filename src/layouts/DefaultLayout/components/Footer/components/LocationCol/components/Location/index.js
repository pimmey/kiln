import React from 'react'
import { object } from 'prop-types'
import styled from 'react-emotion'

import Text from 'Components/ColoredText'
import BoldUppercase from './../../../BoldUppercase'

const LocationAt = styled.div`
  @media only screen and (max-width: 767px) {
    display: none;
  }
`

const LocationInfo = styled.div`
  position: relative;
  z-index: 1;
`

const Location = ({ location }) => (
  <LocationInfo>
    <BoldUppercase>
      <Text.white>{location.name}</Text.white>
    </BoldUppercase>
    <div>
      <div>
        {location.where}
      </div>
      <LocationAt>
        {location.at}
      </LocationAt>
    </div>
  </LocationInfo>
)

Location.propTypes = {
  location: object.isRequired
}

export default Location
