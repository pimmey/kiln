import React from 'react'
import { string, object } from 'prop-types'
import styled from 'react-emotion'
import { Col } from 'react-flexbox-grid'

import Location from './components/Location'

const Img = styled.img`
  height: 150px;
  ${prop => prop.styles.lg};
  
  @media only screen and (max-width: 991px) {
    height: 120px;
  }
  
  @media only screen and (max-width: 767px) {
    ${prop => prop.styles.sm};
  }
`

const LocationCol = ({
  imgSrc,
  styles,
  location,
  alt
}) => (
  <Col xs={6} md={3}>
    <Img src={imgSrc} alt={alt} styles={styles} />
    <Location location={location} />
  </Col>
)

LocationCol.propTypes = {
  imgSrc: string.isRequired,
  styles: object.isRequired,
  location: object.isRequired,
  alt: string.isRequired
}

export default LocationCol
