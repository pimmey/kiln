import React from 'react'
import styled from 'react-emotion'
import { Grid, Row, Col } from 'react-flexbox-grid'

import SocialLinks from 'Components/SocialLinks'

const FooterFootnote = styled.div`
  padding: 29px 0;
  background: rgba(85, 132, 154, .1);
`

const Link = styled.a`
  color: ${prop => prop.theme.colors.opaqueWhite}; 
`

const Small = styled.div`
  margin-top: 5px;
  font-size: 12px;
  color: rgba(255, 255, 255, .3);
`
const Flex = styled(Col)`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  
  @media only screen and (max-width: 767px) {
    justify-content: flex-start;
    margin-top: 18px;
  }
`

const Copyright = styled.span`
  @media only screen and (max-width: 767px) {
    display: none;
  }
`

const Footnote = () => (
  <FooterFootnote>
    <Grid>
      <Row>
        <Col xs={12} md={8}>
          <div>
            <Link href='tel:385-707-5662'>385-707-5662</Link>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <Link href='mailto:hello@kilnspace.com'>hello@kilnspace.com</Link>
          </div>
          <Small>
            <Copyright>
              Copyright © 2017
              &nbsp;&nbsp;
              —
              &nbsp;
            </Copyright>
            <Link href="#!">Terms of Use</Link>
            &nbsp;&nbsp;&nbsp;
            <Link href="#!">Privacy Policy</Link>
            &nbsp;&nbsp;&nbsp;
            <Link href="#!">Cookie Policy</Link>
          </Small>
        </Col>
        <Flex xs={12} md={4}>
          <SocialLinks opaqueWhite />
        </Flex>
      </Row>
    </Grid>
  </FooterFootnote>
)

export default Footnote
