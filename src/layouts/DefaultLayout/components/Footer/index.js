import React from 'react'
import styled from 'react-emotion'
import { Grid, Row } from 'react-flexbox-grid'
import Media from 'react-media'

import Config from 'Utils/config'
import logoSrc from './images/white-logo.svg'
import lehiImgSrc from './images/lehi-building-large.png'
import slcImgSrc from './images/slc-building-large.png'
import Footnote from './components/Footnote'
import FlexEndCol from './components/FlexEndCol'
import JoinKilnCol from './components/JoinKilnCol'
import LocationCol from './components/LocationCol'

const StyledFooter = styled.footer`
  font-size: 14px;
  color: ${prop => prop.theme.colors.opaqueWhite};
  background: ${prop => prop.theme.colors.darkBlue};
  
  @media only screen and (max-width: 767px) {
    overflow: hidden;
  }
`

const Logo = styled.img`
  position: relative;
  top: 5px;
`

const slcBuildingImgStyles = {
  lg: {
    marginLeft: -77
  },
  sm: {
    marginLeft: -65
  }
}

const lehiBuildingImgStyles = {
  lg: {
    position: 'relative',
    top: 10,
    marginLeft: -33
  },
  sm: {
    position: 'relative',
    top: 20,
    marginLeft: 15
  }
}

const PaddedGrid = styled(Grid)`
  padding-top: 29px;
  padding-bottom: 29px;
`

const Footer = () => {
  const {
    Lehi,
    SLC
  } = Config.locations

  return (
    <StyledFooter>
      <PaddedGrid>
        <Row>
          <Media
            query='(min-width: 768px)'
            render={() => (
              <FlexEndCol xs={12} md={3}>
                <Logo src={logoSrc} alt='Kiln' />
              </FlexEndCol>
            )}
          />
          <Media
            query='(max-width: 767px)'
            render={() => <JoinKilnCol />}
          />
          <LocationCol
            imgSrc={slcImgSrc}
            styles={slcBuildingImgStyles}
            location={SLC}
            alt='SLC'
          />
          <LocationCol
            imgSrc={lehiImgSrc}
            styles={lehiBuildingImgStyles}
            location={Lehi}
            alt='Lehi'
          />
          <Media
            query='(min-width: 768px)'
            render={() => <JoinKilnCol />}
          />
        </Row>
      </PaddedGrid>
      <Footnote />
    </StyledFooter>
  )
}

export default Footer
