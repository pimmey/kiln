const PENDING = '_PENDING'
const REJECTED = '_REJECTED'
const FULFILLED = '_FULFILLED'

export default actionName => ({
  base: actionName,
  pending: `${actionName}${PENDING}`,
  rejected: `${actionName}${REJECTED}`,
  fulfilled: `${actionName}${FULFILLED}`
})
