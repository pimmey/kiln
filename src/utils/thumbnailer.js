import store from 'State/store'

import { selectProjectUrl } from 'State/ducks/settings'

const thumbnailer = ({
  src,
  dimensions,
  action = 'contain',
  quality = 'better'
}) => {
  const state = store.getState()
  const projectUrl = selectProjectUrl(state)
  return `${projectUrl}/thumbnail/${dimensions}/${dimensions}/${action}/${quality}/${src}`
}

export default thumbnailer
