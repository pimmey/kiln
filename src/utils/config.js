const Config = {
  mailChimpUrl: '//kilnspace.us17.list-manage.com/subscribe/post-json?u=e58cf01392a67be4185996d37&id=9152b2ee39',
  launchDay: '2018-04-30T06:00:00',
  locations: {
    Lehi: {
      name: 'Lehi',
      where: 'Thanksgiving Station',
      at: 'at North of Vivint Solar',
      when: 'April’18'
    },
    SLC: {
      name: 'SLC',
      where: 'The Gateway',
      at: 'at North Martin Courtyard',
      when: 'Summer’18'
    }
  },
  pages: [
    'Locations',
    'About',
    'News',
    'Events'
  ],
  twitterDateFormat: 'ddd MMM DD HH:mm:ss Z YYYY',
  directusDateFormat: 'YYYY-MM-DD HH:mm:ss',
  validEmailPattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ // eslint-disable-line
}

export default Config
