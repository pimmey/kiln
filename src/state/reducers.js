import { combineReducers } from 'redux'

import subscriptionForm from './ducks/subscriptionForm'
import tweets from './ducks/tweets'
import mobileNav from './ducks/mobileNav'
import news from './ducks/news'
import settings from './ducks/settings'
import events from './ducks/events'
import users from './ducks/users'

export default combineReducers({
  subscriptionForm,
  tweets,
  mobileNav,
  news,
  settings,
  events,
  users
})
