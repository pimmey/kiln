import Client from 'State/utils/Client'

const getEvents = () => Client.getItems('event', { 'order[start_time]': 'ASC' })
  .then(response => response.data)
  .catch(error => console.error(error))

const getEvent = slug => Client.getItems('event', { 'filters[title_slug][eq]': slug })
  .then(response => response.data[0])
  .catch(error => console.error(error))

export default ({
  getEvents,
  getEvent
})
