import { createSelector } from 'reselect'

import makeAsyncAction from 'utils/makeAsyncAction'
import API from './api'

const GET_EVENTS = makeAsyncAction('GET_EVENTS')
const GET_EVENT = makeAsyncAction('GET_EVENT')
const CHOOSE_EVENT = 'CHOOSE_EVENT'

export const selectEvents = state => state.events.list

export const selectFeaturedEvents = createSelector(
  selectEvents,
  list => list.filter(item => item.featured)
)

export const selectSlug = state => state.events.slug

export const selectEventFromList = createSelector(
  selectEvents,
  selectSlug,
  (list, slug) => list.reduce((acc, item) => {
    if (item.title_slug === slug) {
      acc = item
    }
    return acc
  }, {})
)

export const selectEventItem = state => state.events.item

const reducer = (state = {
  list: [],
  item: {},
  slug: null
}, action) => {
  switch (action.type) {
    case GET_EVENTS.fulfilled: {
      return { ...state, list: action.payload }
    }
    case GET_EVENT.fulfilled: {
      return { ...state, item: action.payload }
    }
    case CHOOSE_EVENT: {
      return { ...state, slug: action.payload }
    }
    default: {
      return state
    }
  }
}

export const getEvents = () => dispatch => dispatch({
  type: GET_EVENTS.base,
  payload: API.getEvents()
})

export const getEvent = slug => dispatch => dispatch({
  type: GET_EVENT.base,
  payload: API.getEvent(slug)
})

export const chooseEvent = slug => dispatch => dispatch({
  type: CHOOSE_EVENT,
  payload: slug
})

export default reducer
