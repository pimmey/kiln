import { createAction } from 'redux-actions'

const TOGGLE_MOBILE_NAV = 'TOGGLE_MOBILE_NAV'

export const selectShowMobileNav = state => state.mobileNav.showMobileNav

const reducer = (state = {
  showMobileNav: false
}, action) => {
  switch (action.type) {
    case TOGGLE_MOBILE_NAV: {
      return { ...state, showMobileNav: !state.showMobileNav }
    }
    default: {
      return state
    }
  }
}

export const toggleMobileNav = createAction(TOGGLE_MOBILE_NAV)

export default reducer
