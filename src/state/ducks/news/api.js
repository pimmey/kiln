import Client from 'State/utils/Client'

const getNews = () => Client.getItems('news', { 'order[post_time]': 'DESC' })
  .then(response => response.data)
  .catch(error => console.error(error))

const getArticle = slug => Client.getItems('news', { 'filters[title_slug][eq]': slug })
  .then(response => response.data[0])
  .catch(error => console.error(error))

const getSocialFeed = () => Client.getItems('social_media', { 'order[post_time]': 'DESC' })
  .then(response => response.data)
  .catch(error => console.error(error))

export default ({
  getNews,
  getArticle,
  getSocialFeed
})
