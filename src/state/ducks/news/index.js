import { createSelector } from 'reselect'

import makeAsyncAction from 'Utils/makeAsyncAction'
import API from './api'

const GET_NEWS = makeAsyncAction('GET_NEWS')
const GET_SOCIAL_FEED = makeAsyncAction('GET_SOCIAL_FEED')
const GET_ARTICLE = makeAsyncAction('GET_ARTICLE')
const CHOOSE_ARTICLE = 'CHOOSE_ARTICLE'

export const selectNews = state => state.news.list

export const selectFeaturedNews = createSelector(
  selectNews,
  list => list.filter(item => item.featured)
)

export const selectNewsWithoutFeaturedTwo = createSelector(
  selectNews,
  selectFeaturedNews,
  (list, featuredList) => list.filter(item => item.id !== featuredList[0].id && item.id !== featuredList[1].id)
)

export const selectSocialFeed = state => state.news.socialFeed

export const selectSlug = state => state.news.slug

export const selectArticle = createSelector(
  selectNews,
  selectSlug,
  (list, slug) => list.reduce((acc, item) => {
    if (item.title_slug === slug) {
      acc = item
    }
    return acc
  }, {})
)

const reducer = (state = {
  list: [],
  socialFeed: [],
  slug: null
}, action) => {
  switch (action.type) {
    case GET_NEWS.fulfilled: {
      return { ...state, list: action.payload }
    }
    case GET_SOCIAL_FEED.fulfilled: {
      return { ...state, socialFeed: action.payload }
    }
    case GET_ARTICLE.fulfilled: {
      return { ...state, article: action.payload }
    }
    case CHOOSE_ARTICLE: {
      return { ...state, slug: action.payload }
    }
    default: {
      return state
    }
  }
}

export const getNews = () => dispatch => dispatch({
  type: GET_NEWS.base,
  payload: API.getNews()
})

export const getSocialFeed = () => dispatch => dispatch({
  type: GET_SOCIAL_FEED.base,
  payload: API.getSocialFeed()
})

export const getArticle = slug => dispatch => dispatch({
  type: GET_ARTICLE.base,
  payload: API.getArticle(slug)
})

export const chooseArticle = slug => dispatch => dispatch({
  type: CHOOSE_ARTICLE,
  payload: slug
})

export default reducer
