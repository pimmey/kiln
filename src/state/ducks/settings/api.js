import Client from 'State/utils/client'

const getSettings = () => Client.getSettings()
  .then(response => response.data)
  .catch(error => console.error(error))

export default ({
  getSettings
})
