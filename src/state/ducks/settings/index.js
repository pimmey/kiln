import makeAsyncAction from 'utils/makeAsyncAction'
import API from './api'

const GET_SETTINGS = makeAsyncAction('GET_SETTINGS')

export const selectProjectUrl = state => state.settings.projectUrl

const reducer = (state = {
  projectUrl: {}
}, action) => {
  switch (action.type) {
    case GET_SETTINGS.fulfilled: {
      return { ...state, projectUrl: action.payload.global.project_url }
    }
    default: {
      return state
    }
  }
}

export const getSettings = () => dispatch => dispatch({
  type: GET_SETTINGS.base,
  payload: API.getSettings()
})

export default reducer
