import { createAction } from 'redux-actions'

const FORM_UPDATE_VALUE = 'FORM_UPDATE_VALUE'

export const selectValues = state => state.subscriptionForm.values

const reducer = (state = {
  values: {}
}, action) => {
  switch (action.type) {
    case FORM_UPDATE_VALUE: {
      return { ...state, values: { ...state.values, [action.payload.name]: action.payload.value } }
    }
    default: {
      return state
    }
  }
}

export const updateField = createAction(FORM_UPDATE_VALUE)

export default reducer
