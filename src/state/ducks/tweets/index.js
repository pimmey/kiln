import makeAsyncAction from 'utils/makeAsyncAction'
import API from './api'

const FETCH_TWEETS = makeAsyncAction('FETCH_TWEETS')

export const selectTweets = state => state.tweets.list

const reducer = (state = {
  list: []
}, action) => {
  switch (action.type) {
    case FETCH_TWEETS.fulfilled: {
      return { ...state, list: action.payload }
    }
    default: {
      return state
    }
  }
}

export const fetchTweets = () => dispatch => dispatch({
  type: FETCH_TWEETS.base,
  payload: API.fetchTweets()
})

export default reducer
