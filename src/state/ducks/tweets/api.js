import axios from 'axios'

const {
  protocol,
  hostname
} = window.location

const port = hostname === 'localhost' ? ':8080' : ''
const apiUrl = `${protocol}//${hostname}${port}/api`

const fetchTweets = () => axios
  .get(`${apiUrl}/tweets`)
  .then(response => response.data)

export default ({
  fetchTweets
})
