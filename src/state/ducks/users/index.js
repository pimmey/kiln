import makeAsyncAction from 'Utils/makeAsyncAction'
import API from './api'

const GET_USER = makeAsyncAction('GET_USER')

export const selectUser = state => state.users.user

const reducer = (state = {
  user: {}
}, action) => {
  switch (action.type) {
    case GET_USER.fulfilled: {
      return { ...state, user: action.payload }
    }
    default: {
      return state
    }
  }
}

export const getUser = ({ id }) => dispatch => dispatch({
  type: GET_USER.base,
  payload: API.getUser(id)
})

export default reducer
