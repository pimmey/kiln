import Client from 'State/utils/Client'

const getUser = id => Client.getUser(id)
  .then(response => response.data)
  .catch(error => console.error(error))

export default ({
  getUser
})
