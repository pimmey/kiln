import { css } from 'emotion'

import KontrapunktMikiRegularOtf from './KontrapunktMiki-Regular.otf'
import KontrapunktMikiRegularTtf from './KontrapunktMiki-Regular.ttf'
import KontrapunktMikiRegularWoff from './KontrapunktMiki-Regular.woff'

import KontrapunktMikiBoldOtf from './KontrapunktMiki-Bold.otf'
import KontrapunktMikiBoldTtf from './KontrapunktMiki-Bold.ttf'
import KontrapunktMikiBoldWoff from './KontrapunktMiki-Bold.woff'

import KontrapunktMikiExtraBoldOtf from './KontrapunktMiki-ExtraBold.otf'
import KontrapunktMikiExtraBoldTtf from './KontrapunktMiki-ExtraBold.ttf'
import KontrapunktMikiExtraBoldWoff from './KontrapunktMiki-ExtraBold.woff'

const KontrapunktMiki = css`
  @font-face {
    font-family: 'Kontrapunkt Miki';
    font-style: normal;
    font-weight: 400;
    src:
      url(${KontrapunktMikiRegularOtf}) format('opentype'),
      url(${KontrapunktMikiRegularTtf}) format('truetype'),
      url(${KontrapunktMikiRegularWoff}) format('woff');
  }
  
  @font-face {
    font-family: 'Kontrapunkt Miki';
    font-style: normal;
    font-weight: bold;
    src:
      url(${KontrapunktMikiBoldOtf}) format('opentype'),
      url(${KontrapunktMikiBoldTtf}) format('truetype'),
      url(${KontrapunktMikiBoldWoff}) format('woff');
  }
  
  @font-face {
    font-family: 'Kontrapunkt Miki';
    font-style: normal;
    font-weight: 800;
    src:
      url(${KontrapunktMikiExtraBoldOtf}) format('opentype'),
      url(${KontrapunktMikiExtraBoldTtf}) format('truetype'),
      url(${KontrapunktMikiExtraBoldWoff}) format('woff');
  }
`

export default KontrapunktMiki
