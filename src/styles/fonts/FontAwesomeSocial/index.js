import { css } from 'emotion'

import FontAwesomeSocialTtf from './FontAwesomeSocial.ttf'
import FontAwesomeSocialWoff from './FontAwesomeSocial.woff'
import FontAwesomeSocialSvg from './FontAwesomeSocial.svg'

const FontAwesomeSocial = css`
 @font-face {
    font-family: 'FontAwesomeSocial';
    src:
      url(${FontAwesomeSocialTtf}) format('truetype'),
      url(${FontAwesomeSocialWoff}) format('woff'),
      url(${FontAwesomeSocialSvg}) format('svg');
    font-weight: normal;
    font-style: normal;
  }
  
  [class^="icon-"], [class*=" icon-"] {
    font-family: 'FontAwesomeSocial' !important;
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  
  .icon-twitter:before {
    content: "\\f099";
  }
  .icon-google-plus:before {
    content: "\\f0d5";
  }
  .icon-linkedin:before {
    content: "\\f0e1";
  }
  .icon-facebook:before {
    content: "\\f230";
  }
`

export default FontAwesomeSocial
