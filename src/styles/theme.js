export default({
  colors: {
    darkBlue: '#2a3845',
    white: '#fff',
    coral: '#f1b5a6',
    coralHover: '#f5dbba',
    wheat: '#f4f0eb',
    blue: '#578499',
    opaqueWhite: 'rgba(255, 255, 255, .4)',
    opaqueBlue: 'rgba(42, 56, 70, .6)',
    error: '#f13d10',
    yellowish: '#f6dcb8',
    green: '#6eb285'
  },
  sideBarWidth: 320,
  screens: {
    xs: 'min-width: 480px',
    md: 'min-width: 768px',
    lg: 'min-width: 1024px',
    mdOnly: {
      min: '(min-width: 480px)',
      max: '(max-width: 1024px)'
    }
  }
})
