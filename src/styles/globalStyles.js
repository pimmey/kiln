import { injectGlobal } from 'emotion'

import theme from './theme'
import FontAwesomeSocial from './fonts/FontAwesomeSocial'
import KontrapunktMiki from './fonts/KontrapunktMiki'

injectGlobal`
  * {
    box-sizing: border-box;
  }
  
  ${FontAwesomeSocial};
  ${KontrapunktMiki};
 
  body,
  input,
  textarea,
  select,
  button {
    font-family: 'Kontrapunkt Miki', sans-serif;
    font-synthesis: none;
    -moz-font-feature-settings: 'kern';
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    color: ${theme.colors.darkBlue};
  }
  
  @media only screen and (max-width: 991px) {
    html {
      font-size: 14px
    }
    
    .container-fluid {
      padding: 0;
    }
  }
  
  input:focus,
  button:focus {
    outline: 0;
  }
  
  a {
    text-decoration: none;
  }
  
  ::placeholder {
    color: rgba(255, 255, 255, .4);
  }
  
  ul {
    padding: 0;
    margin: 0;
    list-style: none;
  }
  
  @media only screen and (min-width: 768px) {
    div[class^='col-'] {
      padding-right: 10px;
      padding-left: 10px;
    }
  }
  
  @media only screen and (max-width: 767px) {
    .container {
      padding-right: 30px;
      padding-left: 30px;
    }
    
    div[class^='col-'] {
      padding-right: 5px;
      padding-left: 5px;
    }
    
    .ReactModal__Content {
      height: 100%;
      padding-right: 30px !important;
      padding-left: 30px !important;
    }
    
    .ReactModal__Content .ReactModal__Close {
      top: 0;
      right: 0;
    }
  }
  
  .ReactModal__Body--open {
    overflow: hidden;
  }
  
  nobr {
    white-space: nowrap;
  }
  
  @media only screen and (min-width: 1200px) {
    .container {
      width: 992px;
    }
  }
  
  .jquery-slick {
    min-height: 285px;
    margin-left: -5px;
    margin-right: -5px;
  }
  
  .slick-slider {    
    .slick-arrow {
      position: absolute;
      top: -52px;
      width: 32px;
      height: 32px;
      color: ${theme.colors.darkBlue};
      background: transparent;
      border: 1px solid rgba(42, 56, 70, .2);
      border-radius: 50%;
      cursor: pointer;
      
      &.slick-prev {
        left: 20px;
      }
      
      &.slick-next {
        right: 20px;
      }
      
      & i {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translateX(-50%) translateY(-50%);
        font-size: 20px;
      }
    }
  }
  
  .more-news-slider,
  .more-events-slider {
    margin-right: -20px;
    margin-left: -20px;
    
    @media only screen and (max-width: 768px) {
      margin-right: -30px;
      margin-left: -30px;
    }
  }
`
