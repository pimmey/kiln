import { keyframes } from 'react-emotion'

const pulseAnimation = keyframes`
  0%, 100% {
    opacity: 1;
  }
  
  50% {
    opacity: .75;
  }
`

export default ({
  pulseAnimation
})
