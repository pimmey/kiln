import React, { PureComponent } from 'react'
import {
  object,
  array
} from 'prop-types'
import { connect } from 'react-redux'
import styled from 'react-emotion'
import { Grid, Row, Col } from 'react-flexbox-grid'
import Media from 'react-media'

import ResponsiveGrid from 'Components/ResponsiveGrid'
import Layout from 'Layouts/DefaultLayout'
import Share from 'Components/Share'
import Header from './components/Header'
import MoreEvents from './components/MoreEvents'
import {
  selectEventFromList,
  chooseEvent,
  selectEvents,
  getEvents
} from 'State/ducks/events'

const Wrapper = styled.div`
  margin-top: 98px;
  background: ${prop => prop.theme.colors.wheat};
  
  @media only screen and (max-width: 767px) {
    margin-top: 0;
  }
`

const Content = styled.article`
  padding-bottom: 70px;
  font-size: 1.25rem;
  line-height: 1.3;
  
  @media only screen and (max-width: 767px) {
    padding: 0 10px 50px;
    font-size: 16px;
    line-height: 1.3125;
  }
`

const FixedWidthCol = styled(Col)`
  flex-basis: 139px;
  width: 139px;
`

const StyledGrid = styled(Grid)`
  @media only screen and (max-width: 767px) {
    padding: 0;
    overflow: hidden;
  }
`

class Event extends PureComponent {
  componentDidMount () {
    const {
      dispatch,
      match: {
        params: {
          slug
        }
      },
      events
    } = this.props

    if (!events.length) {
      return dispatch(getEvents()).then(() => {
        dispatch(chooseEvent(slug))
        if (this.props.event.eventbrite_url) {
          this.createWidget(this.props.event.eventbrite_url)
        }
      })
    }

    dispatch(chooseEvent(slug))

    // TODO: think about doing this the right way somehow
    setTimeout(() => {
      if (this.props.event.eventbrite_url) {
        this.createWidget(this.props.event.eventbrite_url)
      }
    }, 0)
  }

  componentDidUpdate (prevProps) {
    if (prevProps.event.id !== undefined && this.props.event.id !== prevProps.event.id) {
      if (this.props.event.eventbrite_url) {
        setTimeout(() => this.createWidget(this.props.event.eventbrite_url), 0)
      }
    }
  }

  componentWillReceiveProps (nextProps) {
    this.props.dispatch(chooseEvent(nextProps.match.params.slug))
  }

  createWidget = eventbriteUrl => {
    let eventbriteId

    if (eventbriteUrl) {
      eventbriteId = eventbriteUrl.match(/\d+$/g, '')[0]

      window.EBWidgets.createWidget({
        widgetType: 'checkout',
        eventId: eventbriteId,
        modal: true,
        modalTriggerElementId: `eventbrite-widget-modal-trigger-${eventbriteId}`
      })
    }
  }

  render () {
    const { event } = this.props

    return (
      <Layout>
        <Wrapper>
          <StyledGrid>
            <Row>
              <Header event={event} />
            </Row>
          </StyledGrid>
          <ResponsiveGrid>
            <Row>
              <FixedWidthCol xs={12} md={2}>
                <Media
                  query='(min-width: 768px)'
                  render={() => (
                    <Share
                      url={event.local_url}
                      title={event.title}
                      summary={event.teaser}
                    />
                  )}
                />
              </FixedWidthCol>
              <Col xs={12} md={8}>
                <Content dangerouslySetInnerHTML={{ __html: event.content }} />
              </Col>
            </Row>
          </ResponsiveGrid>
        </Wrapper>
        <Grid>
          <Row>
            <Col xs={12}>
              <MoreEvents id={event.id} />
            </Col>
          </Row>
        </Grid>
      </Layout>
    )
  }
}

Event.propTypes = {
  event: object.isRequired,
  events: array.isRequired
}

export default connect(state => ({
  event: selectEventFromList(state),
  events: selectEvents(state)
}))(Event)
