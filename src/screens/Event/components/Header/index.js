import React, { PureComponent } from 'react'
import { object } from 'prop-types'
import styled from 'react-emotion'
import { Col } from 'react-flexbox-grid'

import thumbnailer from 'Utils/thumbnailer'
import FirstCol from './components/FirstCol'
import ThirdCol from './components/ThirdCol'
// import Modal from './components/Modal'

const FlexCol = styled(Col)`
  display: flex;
  margin: -69px 0 20px;
  
  @media only screen and (max-width: 767px) {
    margin-top: 0;
  }
`

const StyledHeader = styled.div`
  display: flex;
  width: 100%;
  height: 330px;
  background: ${prop => prop.theme.colors.coral};
  
  @media only screen and (max-width: 767px) {
    flex-wrap: wrap;
    height: auto;
  }
`

const Cover = styled.div`
  flex-shrink: 0;
  width: 330px;
  height: 330px;
  background: url(${prop => prop.bgUrl}) no-repeat center;  
  background-size: cover;
  
  @media only screen and (max-width: 767px) {
    width: 50%;
    height: 188px;
  }
`

const Placeholder = styled.div`
  height: 404px;
`

class Header extends PureComponent {
  state = {
    showModal: false
  }

  openModal = e => {
    e.preventDefault()
    this.setState({ showModal: true })
  }

  closeModal = e => {
    e.preventDefault()
    this.setState({ showModal: false })
  }

  render () {
    const {
      event: {
        start_time,
        end_time,
        image,
        title,
        category,
        location,
        eventbrite_url,
        external_registration_url
      }
    } = this.props

    return start_time ? <FlexCol xs={12}>
      <StyledHeader>
        <FirstCol
          startTime={start_time}
          endTime={end_time}
          category={category}
        />
        <Cover bgUrl={thumbnailer({ src: image.data.name, dimensions: 1000 })} />
        <ThirdCol
          title={title}
          fullAddress={location && location.data.full_address}
          handleClick={this.openModal}
          eventbriteUrl={eventbrite_url}
          externalRegistrationUrl={external_registration_url}
        />
      </StyledHeader>
      {/*
      <Modal
        show={this.state.showModal}
        close={this.closeModal}
      />
      */}
    </FlexCol> : <Placeholder />
  }
}

Header.propTypes = {
  event: object.isRequired
}

export default Header
