import React, { PureComponent } from 'react'
import styled from 'react-emotion'
import Media from 'react-media'

import RoundedButton from 'Components/RoundedButton'

const Col = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 29px 38px;
  
  @media only screen and (max-width: 991px) {
    padding: 20px;
  }
  
  @media only screen and (max-width: 767px) {
    width: 100%;
  }
`

const Title = styled.h1`
  margin: 0;
  font-size: 2.5rem;
  font-weight: normal;
  line-height: 1.2;
  color: ${prop => prop.theme.colors.darkBlue};
  
  @media only screen and (max-width: 991px) {
    margin-bottom: 22px;
    font-size: 30px;
  }
`

const RSVP = styled.div`
 display: flex;
 align-items: center;
`

const ButtonsWrapper = styled.div`
  margin-right: 20px;
`

const Address = styled.address`
  font-size: 12px;
  font-style: normal;
  color: ${prop => prop.theme.colors.opaqueBlue};
  
  & p {
    margin: 0;
    line-height: 1.1666666667;
  }
`

class ThirdCol extends PureComponent {
  state = {
    showButton: true
  }

  componentWillUnmount () {
    const widgetModalOverlay = document.getElementById('eventbrite-widget-modal-overlay')
    const widgetStyle = document.querySelector('body style')

    widgetModalOverlay && widgetModalOverlay.parentNode.removeChild(widgetModalOverlay)
    widgetStyle && widgetStyle.parentNode.removeChild(widgetStyle)
  }

  componentWillReceiveProps (nextProps) {
    const widgetModalOverlay = document.getElementById('eventbrite-widget-modal-overlay')
    const widgetStyle = document.querySelector('body style')

    widgetModalOverlay && widgetModalOverlay.parentNode.removeChild(widgetModalOverlay)
    widgetStyle && widgetStyle.parentNode.removeChild(widgetStyle)

    this.setState({
      showButton: false
    }, () => setTimeout(() => this.setState({
      showButton: true
    }), 10))
  }

  extractEventId = eventUrl => eventUrl.match(/\d+$/g, '')[0]

  render () {
    const {
      title,
      fullAddress,
      // handleClick,
      eventbriteUrl,
      externalRegistrationUrl
    } = this.props

    let eventbriteId

    if (eventbriteUrl) {
      eventbriteId = this.extractEventId(eventbriteUrl)
    }

    return (
      <Col>
        <Title>{title}</Title>
        <RSVP>
          {(eventbriteUrl || externalRegistrationUrl) && <ButtonsWrapper>
            {this.state.showButton && <RoundedButton
              id={eventbriteId && `eventbrite-widget-modal-trigger-${eventbriteId}`}
              href={externalRegistrationUrl}
              target={externalRegistrationUrl && '_blank'}
            >
              <Media query='(max-width: 767px)'>
                {matches => matches ? 'Going' : 'RSVP'}
              </Media>
            </RoundedButton>}
          </ButtonsWrapper>}
          <Address dangerouslySetInnerHTML={{ __html: fullAddress }} />
        </RSVP>
      </Col>
    )
  }
}

export default ThirdCol
