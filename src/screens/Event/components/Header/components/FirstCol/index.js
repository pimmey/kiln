import React from 'react'
import styled from 'react-emotion'

import Date from 'Components/EventItem/components/Date'
import Time from 'Components/EventItem/components/Time'

const Col = styled.div`
  display: flex;
  flex-direction: column;
  flex-shrink: 0;
  width: 139px;
  padding: 10px 20px 15px;
  
  @media only screen and (max-width: 767px) {
    width: 50%;
    height: 188px;
    padding-top: 5px;
    background: ${prop => prop.theme.colors.yellowish};
  }
`

const SpaceBetween = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 100%;
  margin-top: 14px;
  font-size: 0.75rem;
  color: ${prop => prop.theme.colors.darkBlue};
  opacity: .5;
  
  @media only screen and (max-width: 767px) {
    margin-top: 60px;
    font-size: 12px;
    opacity: 1;
    text-transform: uppercase;
  }
`

const Flex = styled.div`
  display: flex;
  align-items: baseline;
`

const FromTo = styled.div`
  line-height: 1.1666666667; 
`

const Category = styled.div`
  @media only screen and (max-width: 767px) {
    display: none;
  }
`

const FirstCol = ({ startTime, endTime, category }) => (
  <Col>
    <Date date={startTime} big />
    <SpaceBetween>
      <FromTo>
        <Flex>
          From&nbsp;<Time date={startTime} big />
        </Flex>
        <Flex>
          To&nbsp;<Time date={endTime} big />
        </Flex>
      </FromTo>
      <Category>
        {category && category.data.name}
      </Category>
    </SpaceBetween>
  </Col>
)

export default FirstCol
