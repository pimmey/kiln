import React from 'react'
import styled from 'react-emotion'

const StyledForm = styled.form`
  margin-top: 43px;
`

const Form = ({ children, ...props }) => <StyledForm {...props}>{children}</StyledForm>

export default Form
