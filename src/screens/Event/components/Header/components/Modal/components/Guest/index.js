import React from 'react'
import styled from 'react-emotion'

import RoundedButton from 'Components/RoundedButton'
import Form from './../Form'
import Input from './../Input'

const InvisibleInput = styled.input`
  display: none;
  
  & + label::before {
    position: relative;
    top: 6px;
    margin-right: 5px;
    font-family: 'Material Icons';
    font-size: 24px;
    color: ${prop => prop.theme.colors.coral};
    content: '\\E836';
  }

  &:checked + label::before {
     content: '\\E86C';
  }
`

const Label = styled.label`
  display: inline-block;
  margin: 15px 0 27px;
  font-size: 14px;
  color: rgba(42, 56, 70, .6);
`

const Guest = ({
  onSubmit,
  onChange,
  onBlur,
  fullName,
  company,
  email,
  subscribe,
  submitTouched
}) => (
  <Form
    name='guestForm'
    onSubmit={onSubmit}
  >
    <Input
      name='fullName'
      type='text'
      placeholder='Full name'
      onChange={onChange}
      onBlur={onBlur}
      value={fullName.value}
      className={(fullName.touched || submitTouched) && fullName.class}
    />
    <Input
      name='company'
      type='text'
      placeholder='Company'
      onChange={onChange}
      onBlur={onBlur}
      value={company.value}
      className={(company.touched || submitTouched) && company.class}
    />
    <Input
      name='email'
      type='email'
      placeholder='Email'
      onChange={onChange}
      onBlur={onBlur}
      value={email.value}
      className={(email.touched || submitTouched) && email.class}
    />
    <InvisibleInput
      name='subscribe'
      type='checkbox'
      id='subscribe'
      checked={subscribe.value}
      onChange={onChange}
      value='1'
    />
    <Label htmlFor='subscribe'>Subscribe to Kiln newsletter</Label>
    <RoundedButton>RSVP</RoundedButton>
  </Form>
)

export default Guest
