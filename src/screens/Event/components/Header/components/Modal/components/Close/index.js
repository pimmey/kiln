import React from 'react'
import styled from 'react-emotion'

const Button = styled.button`
  position: absolute;
  top: 20px;
  right: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 45px;
  height: 45px;
  background: ${prop => prop.theme.colors.white};
  border: 0;
  border-radius: 50%;
  cursor: pointer;
`

const Close = ({ ...props }) => (
  <Button className='ReactModal__Close' {...props}>
    <i className='material-icons'>close</i>
  </Button>
)

export default Close
