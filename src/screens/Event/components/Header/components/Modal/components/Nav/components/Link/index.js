import React from 'react'
import { func, string } from 'prop-types'
import styled from 'react-emotion'

const StyledLink = styled.a`
  position: relative;
  font-size: 1.125rem;
  color: ${prop => prop.theme.colors.opaqueBlue};
  
  &.active {
    font-weight: bold;
    color: ${prop => prop.theme.colors.darkBlue};
    
    &::before {
      position: absolute;
      top: -44px;
      left: 0;
      display: block;
      width: 100%;
      height: 2px;
      background: ${prop => prop.theme.colors.darkBlue};
      content: '';
    }
  }
  
  &:not(:last-of-type) {
    margin-right: 15px;
  }
`

const Link = ({ onClick, activeTab, id, label }) => (
  <StyledLink
    href='#!'
    onClick={onClick}
    id={id}
    className={activeTab === id ? 'active' : ''}
  >
    {label}
  </StyledLink>
)

Link.propTypes = {
  onClick: func.isRequired,
  activeTab: string.isRequired,
  id: string.isRequired,
  label: string.isRequired
}

export default Link
