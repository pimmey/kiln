import React from 'react'

import RoundedButton from 'Components/RoundedButton'
import Form from './../Form'
import Input from './../Input'

const Member = ({
  onSubmit,
  onChange,
  onBlur,
  email,
  password,
  submitTouched
}) => (
  <Form
    name='memberForm'
    onSubmit={onSubmit}
  >
    <Input
      name='email'
      type='email'
      placeholder='Email'
      autoComplete='username'
      onChange={onChange}
      onBlur={onBlur}
      value={email.value}
      className={(email.touched || submitTouched) && email.class}
    />
    <Input
      name='password'
      type='password'
      placeholder='Password'
      autoComplete='current-password'
      onChange={onChange}
      onBlur={onBlur}
      value={password.value}
      className={(password.touched || submitTouched) && password.class}
    />
    <RoundedButton>Log in</RoundedButton>
  </Form>
)

export default Member
