import React from 'react'
import { string, func } from 'prop-types'
import styled from 'react-emotion'

import Link from './components/Link'

const StyledNav = styled.nav`
  position: relative;
`

const Nav = ({ activeTab, onClick }) => (
  <StyledNav>
    <Link
      onClick={onClick}
      id='guest'
      label='Guest'
      activeTab={activeTab}
    >Guest</Link>
    <Link
      onClick={onClick}
      id='kiln-member'
      label='Kiln Member'
      activeTab={activeTab}
    >Kiln Member</Link>
  </StyledNav>
)

Nav.propTypes = {
  activeTab: string.isRequired,
  onClick: func.isRequired
}

export default Nav
