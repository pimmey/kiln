import React from 'react'
import styled from 'react-emotion'

const StyledInput = styled.input`
  display: block;
  width: 100%;
  padding-bottom: 7px;
  margin-top: 26px;
  margin-bottom: ${prop => prop.type === 'password' && '27px'};
  border-radius: 0;
  border: 0;
  border-bottom: 1px solid rgba(42, 56, 70, .15);
  
  & ::placeholder {
    color: rgba(42, 56, 70, .4);
  }
  
  &:focus {
    border-bottom-color: ${prop => prop.theme.colors.coral};
  }
  
  &.has-error {
    color: ${prop => prop.theme.colors.error};
    border-bottom-color: ${prop => prop.theme.colors.error};
    
    &::placeholder {
      color: ${prop => prop.theme.colors.error};
    } 
  }
`

const Input = ({ ...props }) => <StyledInput {...props} />

export default Input
