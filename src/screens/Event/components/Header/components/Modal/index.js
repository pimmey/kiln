import React, { PureComponent } from 'react'
import { func, bool } from 'prop-types'
import ReactModal from 'react-modal'

import Close from './components/Close'
import Nav from './components/Nav'
import Guest from './components/Guest'
import Member from './components/Member'
import Config from '../../../../../../utils/config'

const modalStyle = {
  overlay: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(85, 132, 154, .95)'
  },
  content: {
    position: 'unset',
    top: 'auto',
    right: 'auto',
    bottom: 'auto',
    left: 'auto',
    width: '100%',
    maxWidth: 520,
    padding: '45px 60px 50px',
    border: 0,
    borderRadius: 0
  }
}

class Modal extends PureComponent {
  state = {
    activeTab: 'guest',
    guestForm: {
      submitTouched: false,
      fullName: {
        value: '',
        class: null,
        touched: false
      },
      company: {
        value: '',
        class: null,
        touched: false
      },
      email: {
        value: '',
        class: null,
        touched: false
      },
      subscribe: {
        value: true,
        class: null
      }
    },
    memberForm: {
      submitTouched: false,
      email: {
        value: '',
        class: null,
        touched: false
      },
      password: {
        value: '',
        class: null,
        touched: false
      }
    }
  }

  handleSubmit = e => {
    e.preventDefault()
    const {
      target: {
        name: formName
      }
    } = e

    this.setState({
      [formName]: {
        ...this.state[formName],
        submitTouched: true
      }
    }, () => {
      if (formName === 'guestForm') {
        this.setState({
          [formName]: {
            ...this.state[formName],
            fullName: {
              ...this.state[formName].fullName,
              class: this.state[formName].fullName.value ? 'valid' : 'has-error'
            },
            company: {
              ...this.state[formName].company,
              class: this.state[formName].company.value ? 'valid' : 'has-error'
            },
            email: {
              ...this.state[formName].email,
              class: this.state[formName].email.value ? 'valid' : 'has-error'
            }
          }
        })
      }

      if (formName === 'memberForm') {
        this.setState({
          [formName]: {
            ...this.state[formName],
            email: {
              ...this.state[formName].email,
              class: this.state[formName].email.value ? 'valid' : 'has-error'
            },
            password: {
              ...this.state[formName].password,
              class: this.state[formName].password.value ? 'valid' : 'has-error'
            }
          }
        })
      }
    })
  }

  handleChange = e => {
    const {
      target: {
        name,
        type,
        checked,
        value: inputValue,
        parentNode: {
          name: formName
        }
      }
    } = e

    const value = type === 'checkbox' ? checked : inputValue

    this.setState({
      [formName]: {
        ...this.state[formName],
        [name]: {
          ...this.state[formName][name],
          value
        }
      }
    }, () => {
      if (type === 'email') {
        this.validateEmail(formName, value)
      }

      if (type === 'text' || type === 'password') {
        this.validateTextField(formName, name, value)
      }
    })
  }

  handleBlur = e => {
    const {
      target: {
        name,
        parentNode: {
          name: formName
        }
      }
    } = e

    this.setState({
      [formName]: {
        ...this.state[formName],
        [name]: {
          ...this.state[formName][name],
          touched: true
        }
      }
    })
  }

  handleTabClick = e => {
    e.preventDefault()
    const { id } = e.target
    this.setState({ activeTab: id })
  }

  validateTextField = (formName, name, value) => {
    this.setState({
      [formName]: {
        ...this.state[formName],
        [name]: {
          ...this.state[formName][name],
          class: value.length > 0 ? 'valid' : 'has-error'
        }
      }
    })
  }

  validateEmail = (formName, value) => {
    const re = Config.validEmailPattern

    this.setState({
      [formName]: {
        ...this.state[formName],
        email: {
          ...this.state[formName]['email'],
          class: re.test(value) ? 'valid' : 'has-error'
        }
      }
    })
  }

  render () {
    const {
      show,
      close
    } = this.props

    const {
      guestForm,
      memberForm,
      activeTab
    } = this.state

    const formEvents = {
      onSubmit: this.handleSubmit,
      onBlur: this.handleBlur,
      onChange: this.handleChange
    }

    return (
      <ReactModal
        isOpen={show}
        onRequestClose={close}
        style={modalStyle}
        ariaHideApp={false}
      >
        <Close onClick={close} />
        <Nav
          activeTab={activeTab}
          onClick={this.handleTabClick}
        />
        {activeTab === 'guest'
          ? <Guest {...formEvents} {...guestForm} />
          : <Member {...formEvents} {...memberForm} />}
      </ReactModal>
    )
  }
}

Modal.propTypes = {
  show: bool.isRequired,
  close: func.isRequired
}

export default Modal
