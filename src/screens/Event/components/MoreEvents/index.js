import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import styled from 'react-emotion'
import { Row, Col } from 'react-flexbox-grid'
import Media from 'react-media'

import ResponsiveGrid from 'Components/ResponsiveGrid'
import Title from 'Components/AllCapsTitle'
import EventItem from 'Components/EventItem'
import NewsletterForm from 'Components/NewsletterForm'
import { selectEvents } from 'State/ducks/events'

import $ from 'jquery'
import 'slick-carousel'

const Wrapper = styled.div`
  margin-bottom: 20px;
  
  @media only screen and (max-width: 767px) {
    margin-bottom: 10px;
  }
`

const eventItemStyles = {
  lg: {
    margin: '0 10px'
  },
  sm: {
    margin: '0 5px'
  }
}

class MoreEvents extends PureComponent {
  componentDidMount () {
    this.initSlickSlider()
  }

  componentDidUpdate () {
    this.initSlickSlider()
  }

  componentWillUpdate () {
    $(this.sliderContainer).slick('unslick')
  }

  componentWillUnmount () {
    $(this.sliderContainer).slick('unslick')
  }

  initSlickSlider = () => $('#more-events-slider').slick({
    dots: false,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 2,
    prevArrow: '<button class="slick-arrow slick-prev"><i class="material-icons">keyboard_arrow_left</i></button>',
    nextArrow: '<button class="slick-arrow slick-next"><i class="material-icons">keyboard_arrow_right</i></button>',
    infinite: true
  })

  render () {
    const {
      firstEvent,
      remaining
    } = this.props

    return (
      <Wrapper>
        <Title>More events</Title>
        <ResponsiveGrid>
          <Row>
            <Col xs={12} md={12}>
              <div
                id='more-events-slider'
                className='jquery-slider more-events-slider'
                ref={sliderContainer => this.sliderContainer = sliderContainer}
              >
                {firstEvent.map(event => (
                  <div key={event.id}>
                    <EventItem styles={eventItemStyles} {...event} />
                  </div>
                ))}
                <div>
                  <Media
                    query='(min-width: 768px)'
                    render={() => (
                      <NewsletterForm
                        styles={eventItemStyles}
                        horizontal
                        title='Get events to your inbox'
                        message='Sign up to our newsletter and be first to get nofitied about upcoming events'
                        mailChimpUrl='//kilnspace.us17.list-manage.com/subscribe/post-json?u=e58cf01392a67be4185996d37&id=817eaea4f5'
                      />
                    )}
                  />
                  <Media
                    query='(max-width: 767px)'
                    render={() => (
                      <NewsletterForm
                        eventsSubscribe
                        styles={eventItemStyles}
                        title='Get events to your inbox'
                        message='Sign up to our newsletter and be first to get nofitied about upcoming events'
                        mailChimpUrl='//kilnspace.us17.list-manage.com/subscribe/post-json?u=e58cf01392a67be4185996d37&id=817eaea4f5'
                      />
                    )}
                  />
                </div>
                {remaining.map(event => (
                  <div key={event.id}>
                    <EventItem styles={eventItemStyles} {...event} />
                  </div>
                ))}
              </div>
            </Col>
          </Row>
        </ResponsiveGrid>
      </Wrapper>
    )
  }
}

export default connect((state, { id }) => {
  const selectedEvents = selectEvents(state).filter(article => article.id !== id)

  return {
    firstEvent: selectedEvents.filter((item, index) => index === 0),
    remaining: selectedEvents.filter((item, index) => index > 0)
  }
})(MoreEvents)
