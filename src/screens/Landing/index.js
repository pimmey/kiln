import React from 'react'

import Main from './components/Main'
import Sidebar from './components/Sidebar'

const Landing = () => (
  <div>
    <Main />
    <Sidebar />
  </div>
)

export default Landing
