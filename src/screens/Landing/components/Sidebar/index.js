import React from 'react'
import styled from 'react-emotion'

import SocialLinks from 'Components/SocialLinks'
import SocialLinksWrapper from 'Components/SocialLinksWrapper'
import ContactInfo from 'Components/ContactInfo'
import SubscriptionForm from 'Components/SubscriptionForm'
import Locations from 'Components/Locations'
import { SpaceBetweenCenter } from 'Components/Flex'

const Aside = styled.aside`
  position: fixed;
  top: 0;
  right: 0;
  display: none;
  width: ${prop => prop.theme.sideBarWidth}px;
  height: 100%;
  padding: 40px;
  color: ${props => props.theme.colors.white};
  background: ${props => props.theme.colors.darkBlue};
  overflow-y: scroll;
  overflow-x: hidden;
  -ms-overflow-style: -ms-autohiding-scrollbar;
  
  @media (${prop => prop.theme.screens.lg}) {
    display: block;
  }
`

const Sidebar = () => (
  (
    <Aside>
      <SubscriptionForm formLocation='sidebar' />
      <Locations />
      <SpaceBetweenCenter>
        <ContactInfo />
        <SocialLinksWrapper>
          <SocialLinks />
        </SocialLinksWrapper>
      </SpaceBetweenCenter>
    </Aside>
  )
)

export default Sidebar
