import React from 'react'
import styled from 'react-emotion'

import SocialLinks from 'Components/SocialLinks'
import SocialLinksWrapper from 'Components/SocialLinksWrapper'
import ContactInfo from 'Components/ContactInfo'
import lehiImgSrc from 'Images/building-lehi.png'
import slcImgSrc from 'Images/building-slc.png'
import Config from 'Utils/config'

const StyledFooter = styled.footer`
  display: flex;
  justify-content: space-between;
  padding: 35px 64px;
  background: ${prop => prop.theme.colors.darkBlue};
  
  @media screen and (max-width: 767px) {
    padding: 35px 25px;
  }
`

const Img = styled.img`
  display: block;
  width: 190px;
  margin: 10px auto 0;
`
const LocationInfo = styled.div`
  font-size: 12px;
  line-height: 14px;
  color: ${prop => prop.theme.colors.opaqueWhite};
  
  & span {
    display: block;
    
    &:first-of-type {
      margin-bottom: 14px;
    }
  }
`

const Flex = styled.div`
  display: flex;
`

const Location = styled.div`
  width: 190px;
  
  &:first-of-type {
    margin-right: 70px;
  }
`

const LocationTitle = styled.strong`
  color: ${prop => prop.theme.colors.white};
`

// TODO: unify with Locations/index.js
const Footer = () => {
  const {
    Lehi,
    SLC
  } = Config.locations

  return (
    <StyledFooter>
      <Flex>
        <Location>
          <LocationTitle>Lehi</LocationTitle>
          <Img src={lehiImgSrc} alt='Lehi' />
          <LocationInfo>
            <span>
              {Lehi.where}
              <br />
              {Lehi.at}
            </span>
            <span>
              —&nbsp;{Lehi.when}
            </span>
          </LocationInfo>
        </Location>
        <Location>
          <LocationTitle>SLC</LocationTitle>
          <Img src={slcImgSrc} alt='SLC' />
          <LocationInfo>
            <span>
              {SLC.where}
              <br />
              {SLC.at}
            </span>
            <span>
              —&nbsp;{SLC.when}
            </span>
          </LocationInfo>
        </Location>
      </Flex>
      <div>
        <ContactInfo />
        <SocialLinksWrapper>
          <SocialLinks />
        </SocialLinksWrapper>
      </div>
    </StyledFooter>
  )
}

export default Footer
