import React from 'react'
import { Grid, Row, Col } from 'react-flexbox-grid'

import PaddedSection from 'Components/PaddedSection'
import Heading from 'Components/Heading'
import FeatureBlock from 'Components/FeatureBlock'

const items = [
  {
    title: 'Mentors',
    text: 'Highly curated group of mentors and experts matched to members on a monthly basis through scheduled offices hours.'
  },
  {
    title: 'Investors',
    text: 'A bespoke investor program with monthly match-making and quarterly demo days including out-of-state VC network.'
  },
  {
    title: 'Corporates',
    text: 'A match-making program between start-ups and corporates to enable co-creation and client procurement.'
  }
]

const Growth = () => (
  <PaddedSection white>
    <Grid fluid>
      <Row>
        <Col xs={12}>
          <Heading
            title='Growth'
            subtitle='Kiln accelerates growth by enabling access to insight, capital, customers, talent, and new markets.'
            subtitleWidth={435}
          />
        </Col>
      </Row>
      <Row>
        {items.map(({ ...item }) => (
          <Col
            key={item.title}
            xs={12}
            md={4}
          >
            <FeatureBlock {...item} />
          </Col>
        ))}
      </Row>
    </Grid>
  </PaddedSection>
)

export default Growth
