/* eslint-disable camelcase */
import React from 'react'
import styled from 'react-emotion'
import moment from 'moment/moment'
import Linkify from 'react-linkify'

import Config from 'Utils/config'

const StyledTweet = styled.div`
  position: relative;
  height: 250px;
  padding: 16px 20px;
  margin: 0 5px;
  font-size: 14px;
  line-height: 1.2142857143;
  background: ${prop => prop.theme.colors.white};
  overflow: hidden;
  
  & time,
  & .social-network {
    display: block;
    font-size: 10px;
    font-weight: bold;
    text-transform: uppercase;
    letter-spacing: 1px;
    line-height: 14px;
  }
  
  & time {
    color: ${prop => prop.theme.colors.blue};
  }
  
  & .social-network {
    color: ${prop => prop.theme.colors.coral};
  }
  
  & a {
    color: ${prop => prop.theme.colors.blue};
  }
`

const TwitterImg = styled.img`
  max-width: 100%;
  max-height: 175px;
  margin-top: 14px;
`

const Tweet = ({ created_at, imageSrc, id_str, text }) => (
  <StyledTweet>
    <time>{moment(created_at, Config.twitterDateFormat).format('MMM D')}</time>
    <span className='social-network'>Twitter</span>
    {imageSrc && (
      <a href={`https://twitter.com/kilnspace/status/${id_str}`} target='_blank'>
        <TwitterImg src={imageSrc} alt='Twitter media' />
      </a>
    )}
    <p style={{
      height: imageSrc ? 18 : 'auto',
      overflow: imageSrc ? 'hidden' : 'visible'
    }}>
      <Linkify properties={{target: '_blank'}}>{text}</Linkify>
    </p>
  </StyledTweet>
)

export default Tweet
