import React from 'react'
import styled from 'react-emotion'
import SlickSlider from 'react-slick'
import 'slick-carousel/slick/slick.css'

const StyledSlider = styled(SlickSlider)`
  margin-bottom: 60px;
  margin-left: -5px;
  
  & .slick-dots {
    text-align: center;
    
    & li {
      display: inline-block;
      
      &.slick-active button::after {
        background: ${prop => prop.theme.colors.blue};
      }
    }
    
    & button {
      width: 10px;
      height: 10px;
      color: transparent;
      background: none;
      border: 0;
      
      &::after {
        display: block;
        width: 6px;
        height: 6px;
        border-radius: 50%;
        background: #f5dbba;
        content: '';
      }
    } 
  }
`

const ArrowButton = styled.button`
  position: absolute;
  top: -58px;
  color: ${prop => prop.theme.colors.blue};
  background: transparent;
  border: 0;
  cursor: pointer;
  
  &.slick-next {
    right: 0;
  }
  
  &.slick-prev {
    right: 30px;
  }
`

const PrevButton = ({ ...props }) => (
  <ArrowButton {...props}>
    <i className='material-icons'>arrow_back</i>
  </ArrowButton>
)

const NextButton = ({ ...props }) => (
  <ArrowButton {...props}>
    <i className='material-icons'>arrow_forward</i>
  </ArrowButton>
)

const settings = {
  dots: false,
  infinite: false,
  speed: 500,
  slidesToShow: 4,
  slidesToScroll: 1,
  prevArrow: <PrevButton />,
  nextArrow: <NextButton />,
  responsive: [{
    breakpoint: 768,
    settings: {
      slidesToShow: 1,
      variableWidth: true,
      arrows: false,
      dots: true
    }
  }, {
    breakpoint: 1024,
    settings: {
      slidesToShow: 3
    }
  }]
}

const Slider = ({ children }) => <StyledSlider {...settings}>{children}</StyledSlider>

export default Slider
