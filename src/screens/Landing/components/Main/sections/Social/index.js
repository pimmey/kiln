/* eslint-disable camelcase */
import React, { PureComponent } from 'react'
import { array } from 'prop-types'
import { connect } from 'react-redux'
import styled from 'react-emotion'
import { Grid, Row, Col } from 'react-flexbox-grid'

import {
  fetchTweets,
  selectTweets
} from 'State/ducks/tweets'
import PaddedSection from 'Components/PaddedSection'
import Heading from 'Components/Heading'
import Slider from './components/Slider'
import Tweet from './components/Tweet'

const StyledRow = styled(Row)`
  @media only screen and (max-width: 767px) {
    margin-right: -25px;
    margin-left: -15px;
  }
`

const StyledCol = styled(Col)`
  @media only screen and (max-width: 767px) {
    padding: 0;
  }  
`

class Social extends PureComponent {
  componentDidMount () {
    this.props.dispatch(fetchTweets())
  }

  render () {
    const { tweets } = this.props

    return (
      <PaddedSection>
        <Grid fluid>
          <Row>
            <Col xs={12}>
              <Heading
                title='Social'
                subtitle='Follow us on social media to see how Kiln is coming together.'
              />
            </Col>
          </Row>
          <StyledRow>
            <StyledCol xs={12}>
              <Slider>
                {tweets.map(({
                  id,
                  id_str,
                  created_at,
                  text,
                  extended_entities
                }) => {
                  let imageSrc = null

                  if (extended_entities) {
                    imageSrc = extended_entities.media.map(item => item.media_url_https)[0]
                  }

                  return (
                    <div key={id} style={{ width: 207 }}>
                      <Tweet
                        id={id}
                        id_str={id_str}
                        created_at={created_at}
                        text={text}
                        imageSrc={imageSrc}
                      />
                    </div>
                  )
                })}
              </Slider>
            </StyledCol>
          </StyledRow>
        </Grid>
      </PaddedSection>
    )
  }
}

Social.propTypes = {
  tweets: array
}

export default connect(state => ({
  tweets: selectTweets(state)
}))(Social)
