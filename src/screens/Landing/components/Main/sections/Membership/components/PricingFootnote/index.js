import React from 'react'
import styled from 'react-emotion'

const SmallText = styled.p`
  position: relative;
  top: -44px;
  padding-top: 10px;
  font-size: 12px;
  color: rgba(0, 0, 0, .4);
  
  @media only screen and (max-width: 767px) {
    top: -20px
  }
`

const PricingFootnote = () => (
  <SmallText>
    *Price above is a per person rate. Private office prices will vary upon location and size.
  </SmallText>
)

export default PricingFootnote
