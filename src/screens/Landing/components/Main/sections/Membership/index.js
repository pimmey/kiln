import React from 'react'
import { Grid, Row, Col } from 'react-flexbox-grid'

import PaddedSection from 'Components/PaddedSection'
import Heading from 'Components/Heading'
import FeatureBlock from 'Components/FeatureBlock'
import PricingFootnote from './components/PricingFootnote'

const packages = [
  {
    title: 'Private',
    text: 'A private office from 1 to 50 people.  Enclosed, lockable, ready for your team to quickly move-in and call it home.  Ready for your brand.',
    price: 445,
    variable: true
  },
  {
    title: 'Resident',
    text: 'A dedicated desk to set up base camp.  Perfect for an individual or a growing team. Premium lockable desk. A comfy and fun way to work.',
    price: 375,
    variable: true
  },
  {
    title: 'Club',
    text: 'Drop in and grab an open desk.  An easy way to set up shop. Drop in and work when you like from a shared desk. Very collaborative and agile.',
    price: 195
  },
  {
    title: 'Community',
    text: 'Come to our events to connect, create and launch.  A savvy way to link and learn with the kiln community for free!  Candidly — a no brainer.',
    price: 0
  }
]

const Membership = () => (
  <PaddedSection>
    <Grid fluid>
      <Row>
        <Col xs={12}>
          <Heading
            title='Membership'
            subtitle='Get access to a community and environment designed to accelerate growth.'
          />
        </Col>
      </Row>
      <Row>
        {packages.map(({ ...item }) => (
          <Col
            key={item.title}
            xs={12}
            md={3}
          >
            <FeatureBlock {...item} />
          </Col>
        ))}
      </Row>
      <Row>
        <Col xs={12}>
          <PricingFootnote />
        </Col>
      </Row>
    </Grid>
  </PaddedSection>
)

export default Membership
