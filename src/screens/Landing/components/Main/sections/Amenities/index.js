import React from 'react'
import styled from 'react-emotion'
import { Grid, Row, Col } from 'react-flexbox-grid'

import Heading from 'Components/Heading'
import ImageSrc from './images/amenities.jpg'
import Amenity from './components/Amenity'

const ColumnBackground = styled(Col)`
  min-height: 432px;
  background: url(${ImageSrc}) no-repeat center;
  background-size: cover;
`

const StretchedRow = styled(Row)`
  margin: 0 -16px;
`

const WhiteColumn = styled(Col)`
  padding: 60px !important;
  background: ${prop => prop.theme.colors.white};
  
  @media only screen and (max-width: 767px) {
    padding: 35px 25px !important;;
  } 
`

const List = styled.ul`  
  @media (${prop => prop.theme.screens.md}) {
    column-count: 2;
  }
`

const ForcedPaddingFluidGrid = styled(Grid)`
  padding-right: 16px !important;
  padding-left: 16px !important;
`

const amenities = [
  {
    icon: 'event_seat',
    label: 'Fully furnished'
  },
  {
    icon: 'phone',
    label: 'Calling booths'
  },
  {
    icon: 'notifications',
    label: 'Reception services'
  },
  {
    icon: 'local_printshop',
    label: 'Printing services'
  },
  {
    icon: 'local_drink',
    label: 'Beverage station'
  },
  {
    icon: 'event',
    label: 'Event space'
  },
  {
    icon: 'build',
    label: 'Maker space'
  },
  {
    icon: 'mic',
    label: 'Podcast studio'
  },
  {
    icon: 'delete',
    label: 'Cleaning services'
  },
  {
    icon: 'account_balance',
    label: 'Lending library'
  },
  {
    icon: 'local_dining',
    label: 'Cafe'
  },
  {
    icon: 'videocam',
    label: 'Filming equipment'
  },
  {
    icon: 'settings',
    label: 'Prototyping library cart'
  },
  {
    icon: 'headset_mic',
    label: 'Virtual meeting space'
  }
]

const Amenities = () => (
  <section>
    <ForcedPaddingFluidGrid fluid>
      <StretchedRow>
        <ColumnBackground xs={12} md={5} />
        <WhiteColumn xs={12} md={7}>
          <Heading
            title='Amenities'
            subtitle='Get your space tailored to your immediate needs so you can focus on doing what you do best.'
          />
          <List>
            {amenities.map(({ ...amentity }) => <Amenity key={amentity.icon} {...amentity} />)}
          </List>
        </WhiteColumn>
      </StretchedRow>
    </ForcedPaddingFluidGrid>
  </section>
)

export default Amenities
