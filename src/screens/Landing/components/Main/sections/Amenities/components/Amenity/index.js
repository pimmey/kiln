import React from 'react'
import { string } from 'prop-types'
import styled from 'react-emotion'

const Icon = styled.i`
  position: relative;
  top: 2px;
  margin-right: 13px;
  font-size: 1rem;
  color: ${prop => prop.theme.colors.coral};
`

const Item = styled.li`
  margin-bottom: 15px;
`

const Amenity = ({ icon, label }) => (
  <Item>
    <Icon className='material-icons'>{icon}</Icon>
    {label}
  </Item>
)

Amenity.propTypes = {
  icon: string.isRequired,
  label: string.isRequired
}

export default Amenity
