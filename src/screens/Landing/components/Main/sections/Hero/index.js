import React, { PureComponent } from 'react'
import styled from 'react-emotion'
import { scroller } from 'react-scroll'

import Button from 'Components/Button'
import Logo from './components/Logo'
import Slogan from './components/Slogan'
import Launch from './components/Launch'
import heroBgSrc from './images/hero-bg.jpg'

const Wrapper = styled.section`
  position: relative;
  height: 660px;
  padding: 0 40px;
  background: ${prop => prop.theme.colors.white};
  
  @media only screen and (max-width: 767px) {
    height: auto;
    padding: 0 10px 10px;
  }
`

const Bg = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: url(${heroBgSrc}) no-repeat center;
  background-size: cover;
  
  @media only screen and (max-width: 767px) {
    position: relative;
    height: 361px;
  }
`

class Hero extends PureComponent {
  handleScroll () {
    scroller.scrollTo('subscription-form', {
      smooth: true,
      offset: -100
    })
  }

  render () {
    return (
      <Wrapper>
        <Logo />
        <Bg />
        <Slogan />
        <Launch />
        <Button onClick={this.handleScroll}>Request early access</Button>
      </Wrapper>
    )
  }
}

export default Hero
