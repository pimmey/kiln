import React from 'react'
import styled from 'react-emotion'
import countdown from 'Utils/countdown'

import Config from 'Utils/config'

const LaunchWrapper = styled.div`
  position: absolute;
  bottom: 0;
  padding: 20px 16px;
  margin-left: 50px;
  font-size: 20px;
  line-height: 1.2;
  background: ${prop => prop.theme.colors.coral};
  
  & em {
    font-style: normal;
    color: ${prop => prop.theme.colors.white};
  }
  
  @media only screen and (max-width: 767px) {
    position: relative;
    font-size: 18px;
    padding: 0 7px;
    margin-top: 18px;
    margin-bottom: 18px;
    margin-left: 0;
    color: ${prop => prop.theme.colors.blue};
    background: none;
    
    & em {
      color: ${prop => prop.theme.colors.darkBlue} !important;
    }
  }
`

const Launch = () => {
  const remaining = countdown(new Date(), new Date(Config.launchDay), 0x040 | 0x010).toHTML('em')

  return (
    <LaunchWrapper>
      Kiln launches in Lehi in <span dangerouslySetInnerHTML={{ __html: remaining }} />.
    </LaunchWrapper>
  )
}

export default Launch
