import React from 'react'
import styled from 'react-emotion'

import sloganBgSrc from './images/slogan-bg.svg'
import mobileSloganBgSrc from './images/mobile-slogan-bg.svg'

const SloganWrapper = styled.h1`
  position: absolute;
  bottom: 64px;
  max-width: 500px;
  height: 164px;
  padding: 6px 0 0 17px;
  margin: 0;
  font-size: 40px;
  font-weight: 400;
  letter-spacing: 2px;
  line-height: 1.2;
  background: url(${sloganBgSrc}) center no-repeat;
  
  @media only screen and (max-width: 767px) {
    position: relative;
    bottom: auto;
    width: 246px;
    height: 128px;
    padding: 2px 0 0 7px;
    margin-top: -68px;
    font-size: 25px;
    letter-spacing: 1.25px;
    color: ${prop => prop.theme.colors.white};
    background: url(${mobileSloganBgSrc}) center no-repeat;
  }
`

const Slogan = () => (
  <SloganWrapper>
    Community that inspires, connects, and accelerates growth
  </SloganWrapper>
)

export default Slogan
