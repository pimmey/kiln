import React from 'react'
import styled from 'react-emotion'

import logoSrc from 'Images/logo.svg'

const Img = styled.img`
  position: relative;
  z-index: 1;
  margin-top: 40px;
  
  @media only screen and (max-width: 767px) {
    width: 79px;
    margin: 20px;
  } 
`

const Logo = () => <Img src={logoSrc} alt='Kiln' />

export default Logo
