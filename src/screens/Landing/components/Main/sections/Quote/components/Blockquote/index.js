import React from 'react'
import styled from 'react-emotion'

import DoubleQuotes from './components/DoubleQuotes'
import Author from './components/Author'

const BlockquoteWrapper = styled.div`
  position: absolute;
  z-index: 1;
  bottom: 0;
  left: 30px;
  
  @media only screen and (max-width: 479px) {
    position: relative;
    left: 0;
  }
`

const StyledBlockquote = styled.blockquote`
  width: 334px;
  padding: 40px 20px 13px;
  margin: 0;
  font-size: 1.25rem;
  line-height: 1.5rem;
  color: ${prop => prop.theme.colors.white};
  background: ${prop => prop.theme.colors.blue};
  
  & p {
    margin: 0;
  }
  
  @media only screen and (max-width: 479px) {
    position: relative;
    width: 100%;
    background: ${prop => prop.theme.colors.darkBlue};
    
    & p {
      width: 200px
    }
  }
`

const Blockquote = () => (
  <BlockquoteWrapper>
    <StyledBlockquote>
      <DoubleQuotes />
      <p>
        Collectively, connected
        humans will be capable
        of things we cannot
        imagine right now.”
      </p>
    </StyledBlockquote>
    <Author />
  </BlockquoteWrapper>
)

export default Blockquote
