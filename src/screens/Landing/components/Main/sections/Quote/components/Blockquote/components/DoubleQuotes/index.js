import React from 'react'
import styled from 'react-emotion'

import doubleQuotesSrc from './images/double-quotes.svg'

const Img = styled.img`
  position: absolute;
  top: 20px;
`

const DoubleQuotes = () => <Img src={doubleQuotesSrc} alt='Double quotes' />

export default DoubleQuotes
