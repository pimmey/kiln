import React from 'react'
import styled from 'react-emotion'

const StyledAuthor = styled.div`
  position: relative;
  left: 247px;
  width: 127px;
  height: 29px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: .75rem;
  font-weight: bold;
  text-transform: uppercase;
  letter-spacing: 1.2px;
  color: ${prop => prop.theme.colors.white};
  background: ${prop => prop.theme.colors.coral};
  
  @media only screen and (max-width: 479px) {
    position: absolute;
    right: 20px;
    left: auto;
   }
`

const Author = () => <StyledAuthor>Kevin Kelly</StyledAuthor>

export default Author
