import React from 'react'
import styled from 'react-emotion'

import bgSrc from './images/kevin-kelly-quote-bg.jpg'
import Blockquote from './components/Blockquote'

const SectionWithBg = styled.section`
  position: relative;
  min-height: 354px;
  
  @media only screen and (max-width: 479px) {
    min-height: auto;
  }
`

const Bg = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: url(${bgSrc}) no-repeat center;
  background-size: cover;
  
  @media only screen and (max-width: 479px) {
    position: relative;
    height: 200px;
  }
`

const Quote = () => (
  <SectionWithBg>
    <Blockquote />
    <Bg />
  </SectionWithBg>
)

export default Quote
