import React from 'react'
import styled from 'react-emotion'
import { Element } from 'react-scroll'
import Media from 'react-media'

import SubscriptionForm from 'Components/SubscriptionForm'
import Footer from './components/Footer'

const Wrapper = styled(Element)`
  padding: 33px 40px 60px;
  background: ${prop => prop.theme.colors.darkBlue};
  
  @media (${prop => prop.theme.screens.lg}) {
    display: none;
  }
`

const RequestAccess = ({ name, id }) => (
  <Wrapper name={name}>
    <SubscriptionForm formLocation='main' id={id} />
    <Media
      query='(max-width: 767px)'
      render={() => <Footer />}
    />
  </Wrapper>
)

export default RequestAccess
