import React from 'react'

import ContactInfo from 'Components/ContactInfo'
import SocialLinks from 'Components/SocialLinks'
import SocialLinksWrapper from 'Components/SocialLinksWrapper'
import Locations from 'Components/Locations'
import { SpaceBetweenCenter } from 'Components/Flex'

const Footer = () => (
  <footer>
    <Locations />
    <SpaceBetweenCenter>
      <ContactInfo />
      <SocialLinksWrapper>
        <SocialLinks />
      </SocialLinksWrapper>
    </SpaceBetweenCenter>
  </footer>
)

export default Footer
