import React from 'react'
import { Grid, Row, Col } from 'react-flexbox-grid'

import FeatureBlock from 'Components/FeatureBlock'
import PaddedSection from 'Components/PaddedSection'

const values = [
  {
    title: 'Community',
    text: 'Providing the ideal network and environment to grow your company in SLC via flexible monthly coworking membership.'
  },
  {
    title: 'Growth',
    text: 'Focused on accelerating growth through a set of resources and programs that enable access to insight, capital, customers, and talent.'
  },
  {
    title: 'Innovation',
    text: 'Helping large corporates to open their doors to startups and co-create with early stage companies via proven models for experimentation.'
  }
]

const Values = () => (
  <PaddedSection>
    <Grid fluid>
      <Row>
        {values.map(({ ...value }) => (
          <Col
            key={value.title}
            xs={12}
            md={4}
          >
            <FeatureBlock {...value} />
          </Col>
        ))}
      </Row>
    </Grid>
  </PaddedSection>
)

export default Values
