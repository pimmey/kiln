import React from 'react'
import styled from 'react-emotion'
import Media from 'react-media'

import Hero from './sections/Hero'
import RequestAccess from './sections/RequestAccess'
import Values from './sections/Values'
import Amenities from './sections/Amenities'
import Membership from './sections/Membership'
import Growth from './sections/Growth'
import Quote from './sections/Quote'
import Social from './sections/Social'
import Footer from './sections/Footer'

const StyledMain = styled.main`
  background: ${prop => prop.theme.colors.wheat};
  
  @media (${prop => prop.theme.screens.lg}) {
    padding-right: ${prop => prop.theme.sideBarWidth}px;
  }
`

const Main = () => (
  <StyledMain>
    <Hero />
    <Media
      query='(min-width: 768px)'
      render={() => <RequestAccess id='tablet-subscription' />}
    />
    <Values />
    <Amenities />
    <Membership />
    <Quote />
    <Growth />
    <Social />
    <Media
      query='(max-width: 767px)'
      render={() => <RequestAccess name='subscription-form' id='mobile-subscription' />}
    />
    <Media
      query='(min-width: 768px) and (max-width: 1023px)'
      render={() => <Footer />}
    />
  </StyledMain>
)

export default Main
