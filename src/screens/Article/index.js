import React, { PureComponent } from 'react'
import {
  array,
  object
} from 'prop-types'
import { connect } from 'react-redux'
import styled from 'react-emotion'
import { Grid, Row, Col } from 'react-flexbox-grid'
import Media from 'react-media'

import ResponsiveGrid from 'Components/ResponsiveGrid'
import Layout from 'layouts/DefaultLayout'
import Share from 'Components/Share'
import Header from './components/Header'
import MoreNews from './components/MoreNews'
import {
  chooseArticle,
  selectArticle,
  selectNews,
  getNews
} from 'State/ducks/news'
import {
  getUser,
  selectUser
} from 'State/ducks/users'

const Wrapper = styled.div`
  margin-top: 98px;
  background: ${prop => prop.theme.colors.wheat};
  
  @media only screen and (max-width: 767px) {
    margin-top: 0;
  }
`

const Content = styled.article`
  padding-bottom: 70px;
  font-size: 1.25rem;
  line-height: 1.3;
  
  @media only screen and (max-width: 767px) {
    padding: 0 10px 50px;
    font-size: 16px;
    line-height: 1.3125;
  }
`

const FixedWidthCol = styled(Col)`
  flex-basis: 139px;
  width: 139px;
`

const StyledGrid = styled(Grid)`
  @media only screen and (max-width: 767px) {
    padding: 0;
    overflow: hidden;
  }
`

class Article extends PureComponent {
  componentDidMount () {
    const {
      dispatch,
      match,
      news
    } = this.props

    if (!news.length) {
      dispatch(getNews())
    }

    dispatch(chooseArticle(match.params.slug))
  }

  componentWillReceiveProps (nextProps) {
    const { dispatch } = this.props

    dispatch(chooseArticle(nextProps.match.params.slug))

    if (nextProps.article.author && (this.props.article.author !== nextProps.article.author)) {
      dispatch(getUser({ id: nextProps.article.author }))
    }
  }

  render () {
    const {
      article,
      author
    } = this.props

    return (
      <Layout>
        <Wrapper>
          <StyledGrid>
            <Row>
              <Header author={author} />
            </Row>
          </StyledGrid>
          <ResponsiveGrid>
            <Row>
              <Media
                query='(min-width: 768px)'
                render={() => (
                  <FixedWidthCol xs={12} md={2}>
                    <Share
                      url={article.local_url}
                      title={article.title}
                      summary={article.teaser}
                    />
                  </FixedWidthCol>
                )}
              />
              <Col xs={12} md={8}>
                <Content dangerouslySetInnerHTML={{ __html: article.content }} />
              </Col>
            </Row>
          </ResponsiveGrid>
        </Wrapper>
        <Grid>
          <Row>
            <Col xs={12}>
              <MoreNews id={article.id} />
            </Col>
          </Row>
        </Grid>
      </Layout>
    )
  }
}

Article.propTypes = {
  author: object.isRequired,
  news: array.isRequired,
  article: object.isRequired
}

export default connect(state => ({
  article: selectArticle(state),
  news: selectNews(state),
  author: selectUser(state)
}))(Article)
