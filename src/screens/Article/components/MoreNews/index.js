import React, { PureComponent } from 'react'
import { array } from 'prop-types'
import { connect } from 'react-redux'
import styled from 'react-emotion'
import { Row, Col } from 'react-flexbox-grid'

import ResponsiveGrid from 'Components/ResponsiveGrid'
import Title from 'Components/AllCapsTitle'
import NewsItem from 'Components/NewsItem'
import NewsletterForm from 'Components/NewsletterForm'
import { selectNews } from 'State/ducks/news'

import $ from 'jquery'
import 'slick-carousel'

const Wrapper = styled.div`
  margin-bottom: 20px;
  
  @media only screen and (max-width: 767px) {
    margin-bottom: 10px;
  }
`

const newsItemStyles = {
  lg: {
    margin: '0 10px'
  },
  sm: {
    margin: '0 5px'
  }
}

class MoreNews extends PureComponent {
  componentDidMount () {
    this.initSlickSlider()
  }

  componentDidUpdate () {
    this.initSlickSlider()
  }

  componentWillUpdate () {
    $(this.sliderContainer).slick('unslick')
  }

  componentWillUnmount () {
    $(this.sliderContainer).slick('unslick')
  }

  initSlickSlider = () => $(this.sliderContainer).slick({
    dots: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    prevArrow: '<button class="slick-arrow slick-prev"><i class="material-icons">keyboard_arrow_left</i></button>',
    nextArrow: '<button class="slick-arrow slick-next"><i class="material-icons">keyboard_arrow_right</i></button>',
    infinite: true,
    responsive: [{
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    }, {
      breakpoint: 992,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    }]
  })

  render () {
    const {
      newsFirstBatch,
      newsSecondBatch
    } = this.props

    return (
      <Wrapper>
        <Title>More news</Title>
        <ResponsiveGrid>
          <Row>
            <Col xs={12} md={12}>
              <div
                id='more-news-slider'
                className='jquery-slick more-news-slider'
                ref={sliderContainer => this.sliderContainer = sliderContainer}
              >
                {newsFirstBatch.map(post => (
                  <div key={post.timestamp}>
                    <NewsItem moreNews styles={newsItemStyles} {...post} />
                  </div>
                ))}
                <div>
                  <NewsletterForm
                    styles={newsItemStyles}
                    horizontal={false}
                    title='Never miss a post'
                    message='Sign up to our newsletter and be on top of what’s happening in technology'
                    mailChimpUrl='//kilnspace.us17.list-manage.com/subscribe/post-json?u=e58cf01392a67be4185996d37&id=817eaea4f5'
                  />
                </div>
                {newsSecondBatch.map(post => (
                  <div key={post.timestamp}>
                    <NewsItem moreNews styles={newsItemStyles} {...post} />
                  </div>
                ))}
              </div>
            </Col>
          </Row>
        </ResponsiveGrid>
      </Wrapper>
    )
  }
}

MoreNews.propTypes = {
  newsFirstBatch: array.isRequired,
  newsSecondBatch: array.isRequired
}

export default connect((state, { id }) => {
  let limit = 3

  if (window.matchMedia('only screen and (max-width: 767px)').matches) {
    limit = 1
  } else if (window.matchMedia('only screen and (min-width: 768px) and (max-width: 991px').matches) {
    limit = 2
  }

  const selectedNews = selectNews(state).filter(article => article.id !== id)

  return {
    newsFirstBatch: selectedNews.slice(0, limit),
    newsSecondBatch: selectedNews.slice(limit, 7)
  }
})(MoreNews)
