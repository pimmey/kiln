import React from 'react'
import { object } from 'prop-types'
import { connect } from 'react-redux'
import styled from 'react-emotion'
import { Col } from 'react-flexbox-grid'

import thumbnailer from 'Utils/thumbnailer'
import Meta from './../../../News/components/Meta'
import { selectArticle } from 'State/ducks/news'

const Info = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: ${prop => prop.noImage ? 100 : 50}%;
  height: ${prop => prop.noImage ? 276 : 330}px;
  padding: 34px 40px;
  background: ${prop => prop.noImage ? prop.theme.colors.blue : prop.theme.colors.green};
  
  @media only screen and (max-width: 767px) {
    width: 100%;
    height: auto;
    height: ${prop => prop.noImage && 'auto'};
    padding: 13px 19px 22px;
  }
`

const Author = styled.div`
  margin-bottom: 5px;
  font-size: 12px;
  font-weight: bold;
  color: ${prop => prop.white && prop.theme.colors.white};
  text-transform: uppercase;
`

const Title = styled.h1`
  max-width: 660px;
  margin: 0;
  font-size: 2.5rem;
  font-weight: normal;
  line-height: 1.2;
  color: ${prop => prop.theme.colors.yellowish};
  
  @media only screen and (max-width: 767px) {
    font-size: 30px;
  }
`

const Cover = styled.div`
  width: 50%;
  height: 330px;
  background: url(${prop => prop.bgUrl}) no-repeat center;  
  background-size: cover;
  
  @media only screen and (max-width: 767px) {
    width: 100%;
    height: 182px;
  }
`

const FlexCol = styled(Col)`
  display: flex;
  margin: -69px 0 20px;
  
  @media only screen and (max-width: 767px) {
    flex-direction: column-reverse;
    margin-top: 0;
  }
`

const Placeholder = styled.div`
  height: 276px;
`

const MetaWrapper = styled.div`
  @media only screen and (max-width: 767px) {
    margin-bottom: 18px;
  }
`

const Header = ({
  article: {
    post_time,
    category,
    title,
    image
  },
  author: {
    first_name,
    last_name
  }
}) => (
  post_time ? <FlexCol xs={12}>
    <Info noImage={image === null}>
      <MetaWrapper>
        <Author white={image === null}>
          {first_name} {last_name}
        </Author>
        <Meta
          white={image === null}
          date={post_time}
          category={category && category.data.name}
        />
      </MetaWrapper>
      <Title>{title}</Title>
    </Info>
    {image && <Cover bgUrl={thumbnailer({ src: image.data.name, dimensions: 750 })} />}
  </FlexCol> : <Placeholder />
)

Header.propTypes = {
  author: object.isRequired,
  article: object.isRequired
}

export default connect(state => ({
  article: selectArticle(state)
}))(Header)
