import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import Media from 'react-media'

import Layout from 'Layouts/DefaultLayout'
import Featured from './components/Featured'
import FeaturedSlider from './components/FeaturedSlider'
import SocialMediaCarousel from './components/SocialMediaCarousel'
import TechnologyNews from './components/TechnologyNews'
import {
  getNews,
  selectNews,
  selectFeaturedNews
} from 'State/ducks/news'

class News extends PureComponent {
  componentDidMount () {
    this.props.dispatch(getNews())
  }

  render () {
    return (
      <Layout>
        <Media
          query='(min-width: 768px)'
          render={() => <Featured />}
        />
        <Media
          query='(max-width: 767px)'
          render={() => <FeaturedSlider />}
        />
        <SocialMediaCarousel />
        <TechnologyNews />
      </Layout>
    )
  }
}

export default connect(state => ({
  news: selectNews(state),
  featuredNews: selectFeaturedNews(state)
}))(News)
