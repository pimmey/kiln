import React from 'react'
import styled from 'react-emotion'
import { NavLink } from 'react-router-dom'

import thumbnailer from 'Utils/thumbnailer'
import Meta from 'Screens/News/components/Meta'

const StyledSlide = styled(NavLink)`
  display: block;
  background: ${prop => prop.theme.colors.yellowish};
`

const Info = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 182px;
  padding: 12px 20px 18px;
`

const Cover = styled.div`
  height: 182px;
  background: url(${prop => prop.bgSrc}) center no-repeat;
  background-size: cover; 
`

const Title = styled.div`
  font-size: 30px;
  line-height: 1.2;
  color: ${prop => prop.featured ? prop.theme.colors.yellowish : prop.theme.colors.darkBlue};
`

const Slide = ({
  local_url,
  image,
  title,
  post_time,
  category
}) => (
  <StyledSlide to={local_url}>
    <Cover bgSrc={thumbnailer({ src: image.data.name, dimensions: 1000 })} />
    <Info>
      <Title>{title}</Title>
      <Meta
        date={post_time}
        category={category && category.data.name}
      />
    </Info>
  </StyledSlide>
)

export default Slide
