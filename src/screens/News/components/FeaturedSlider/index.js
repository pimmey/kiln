import React from 'react'
import { connect } from 'react-redux'

import MobileSlider from 'Components/MobileSlider'
import Slide from './components/Slide'
import { selectFeaturedNews } from 'State/ducks/news'

const FeaturedSlider = ({ featuredNews }) => (
  <MobileSlider>
    {featuredNews.map(item => (
      <div key={item.id}>
        <Slide {...item} />
      </div>
    ))}
  </MobileSlider>
)

export default connect(state => ({
  featuredNews: selectFeaturedNews(state).slice(0, 2)
}))(FeaturedSlider)
