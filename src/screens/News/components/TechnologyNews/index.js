import React, { PureComponent } from 'react'
import { array } from 'prop-types'
import { connect } from 'react-redux'
import { Row, Col } from 'react-flexbox-grid'

import ResponsiveGrid from 'Components/ResponsiveGrid'
import Title from 'Components/AllCapsTitle'
import NewsItem from 'Components/NewsItem'
import NewsletterForm from 'Components/NewsletterForm'
import LoadMore from 'Components/LoadMore'
import { selectNewsWithoutFeaturedTwo } from 'State/ducks/news'

class TechnologyNews extends PureComponent {
  state = {
    firstSliceTo: 0,
    sliceFrom: 0,
    sliceTo: 0,
    initialSliceTo: 0,
    sliceToIncrement: 0
  }

  componentDidMount () {
    this.assignValues()
    window.addEventListener('resize', this.assignValues)
  }

  componentWillUnmount () {
    window.removeEventListener('resize', this.assignValues)
  }

  assignValues = () => {
    if (window.matchMedia('only screen and (max-width: 767px)').matches) {
      this.setState({
        firstSliceTo: 3,
        sliceFrom: 3,
        initialSliceTo: 7,
        sliceToIncrement: 4
      })
    } else if (window.matchMedia('only screen and (min-width: 768px) and (max-width: 991px)').matches) {
      this.setState({
        firstSliceTo: 5,
        sliceFrom: 5,
        initialSliceTo: 11,
        sliceToIncrement: 6
      })
    } else {
      this.setState({
        firstSliceTo: 7,
        sliceFrom: 7,
        initialSliceTo: 15,
        sliceToIncrement: 8
      })
    }
  }

  handleClick = () => {
    const {
      sliceTo,
      initialSliceTo,
      sliceToIncrement
    } = this.state

    this.setState({
      sliceTo: sliceTo === 0 ? initialSliceTo : sliceTo + sliceToIncrement
    })
  }

  render () {
    const {
      firstSliceTo,
      sliceFrom,
      sliceTo
    } = this.state

    const { news } = this.props
    const nothingToLoad = news.length <= sliceTo || news.length <= sliceFrom

    return (
      <ResponsiveGrid>
        <Row>
          <Col xs={12}>
            <Title>Technology news</Title>
          </Col>
        </Row>
        <Row>
          {news.slice(0, firstSliceTo).map(item => (
            <Col key={item.id} xs={6} md={4} lg={3}>
              <NewsItem {...item} />
            </Col>
          ))}
          <Col xs={6} md={4} lg={3}>
            <NewsletterForm
              horizontal={false}
              title='Never miss <nobr>a post</nobr>'
              message='Sign up to our newsletter and be on top of what’s happening in technology'
              mailChimpUrl='//kilnspace.us17.list-manage.com/subscribe/post-json?u=e58cf01392a67be4185996d37&id=817eaea4f5'
            />
          </Col>
          {news.slice(sliceFrom, sliceTo).map((item, index) => (
            <Col key={index} xs={6} md={4} lg={3}>
              <NewsItem index={index} {...item} />
            </Col>
          ))}
          <Col xs={12}>
            {!nothingToLoad && <LoadMore onClick={this.handleClick} />}
          </Col>
        </Row>
      </ResponsiveGrid>
    )
  }
}

TechnologyNews.propTypes = {
  news: array.isRequired
}

export default connect(state => ({
  news: selectNewsWithoutFeaturedTwo(state)
}))(TechnologyNews)
