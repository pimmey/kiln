import React from 'react'
import { Grid, Row, Col } from 'react-flexbox-grid'

import Headline from './components/Headline'
import Secondary from './components/Secondary'

const Featured = () => (
  <Grid>
    <Row>
      <Col xs={12} md={8}>
        <Headline />
      </Col>
      <Col xs={12} md={4}>
        <Secondary />
      </Col>
    </Row>
  </Grid>
)

export default Featured
