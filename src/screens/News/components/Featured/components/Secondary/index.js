import React from 'react'
import { connect } from 'react-redux'
import styled from 'react-emotion'

import thumbnailer from 'Utils/thumbnailer'
import LinkBlock from '../LinkBlock'
import Meta from './../../../Meta'
import { selectFeaturedNews } from 'State/ducks/news'
import Animations from 'Styles/animations'

const Cover = styled.div`
  height: 50%;
  background: url(${prop => prop.bgUrl}) no-repeat center;  
  background-size: cover;
`

const Title = styled.div`
  font-size: 1.5625rem;
  line-height: 1.2;
`

const Description = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 50%;
  padding: 28px 38px;
  color: ${prop => prop.theme.colors.yellowish};
  background: ${prop => prop.theme.colors.green};
`

const Placeholder = styled.div`
  height: 420px;
  background: ${prop => prop.theme.colors.green};
  animation: 2s linear infinite ${Animations.pulseAnimation};
`

// TODO: refactor and unify with Headline/index.js
const Secondary = ({
  secondary: {
    local_url,
    image,
    title,
    post_time,
    category
  }
}) => (
  local_url ? <LinkBlock direction='column' to={local_url}>
    <Cover bgUrl={thumbnailer({ src: image.data.name, dimensions: 750 })} />
    <Description>
      <Title>{title}</Title>
      <Meta
        date={post_time}
        category={category.data.name}
      />
    </Description>
  </LinkBlock> : <Placeholder />
)

export default connect(state => ({
  secondary: selectFeaturedNews(state)[1] || {}
}))(Secondary)
