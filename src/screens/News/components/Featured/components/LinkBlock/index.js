import React from 'react'
import { string } from 'prop-types'
import styled from 'react-emotion'
import { NavLink } from 'react-router-dom'

const StyledLinkBlock = styled(NavLink)`
  display: flex;
  flex-direction: ${prop => prop.direction === 'column' ? 'column' : 'row'};
  height: 420px;
`

const LinkBlock = ({ direction, children, ...props }) => (
  <StyledLinkBlock direction={direction} {...props}>
    {children}
  </StyledLinkBlock>
)

LinkBlock.propTypes = {
  direction: string
}

export default LinkBlock
