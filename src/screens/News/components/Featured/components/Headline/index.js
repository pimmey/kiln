import React from 'react'
import { connect } from 'react-redux'
import styled from 'react-emotion'

import thumbnailer from 'Utils/thumbnailer'
import LinkBlock from '../LinkBlock'
import Meta from './../../../Meta'
import { selectFeaturedNews } from 'State/ducks/news'
import Animations from 'Styles/animations'

const Cover = styled.div`
  width: 50%;
  height: 100%;
  background: url(${prop => prop.bgUrl}) no-repeat center;  
  background-size: cover;
`

const Title = styled.div`
  font-size: 2.5rem;
  line-height: 1.2;
  color: ${prop => prop.theme.colors.darkBlue};
`

const Description = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 50%;
  padding: 28px 38px;
  background: ${prop => prop.theme.colors.yellowish};
`

const Placeholder = styled.div`
  height: 420px;
  background: ${prop => prop.theme.colors.yellowish};
  animation: 2s linear infinite ${Animations.pulseAnimation};
`

const Headline = ({
  headline: {
    local_url,
    image,
    title,
    post_time,
    category
  }
}) => (
  local_url ? <LinkBlock to={local_url} id={777}>
    <Cover bgUrl={thumbnailer({ src: image.data.name, dimensions: 1000 })} />
    <Description>
      <Title>{title}</Title>
      <Meta
        date={post_time}
        category={category.data.name}
      />
    </Description>
  </LinkBlock> : <Placeholder />
)

export default connect(state => ({
  headline: selectFeaturedNews(state)[0] || {}
}))(Headline)
