import React from 'react'
import moment from 'moment'
import {
  string,
  bool
} from 'prop-types'
import styled from 'react-emotion'

const StyledMeta = styled.div`
  font-size: 12px;
  color: ${prop => prop.white ? prop.theme.colors.opaqueWhite : prop.theme.colors.opaqueBlue};
`

const Meta = ({ date, category, white }) => (
  <StyledMeta white={white}>
    <time>{moment(date, 'YYYY-MM-DD HH:mm:ss').calendar(null, {
      sameDay: '[Today]',
      lastDay: '[Yesterday]',
      lastWeek: 'MMM DD',
      sameElse: 'MMM DD'
    })}</time>
    {category && <span>&nbsp;&nbsp;&nbsp;—&nbsp;&nbsp;{category}</span>}
  </StyledMeta>
)

Meta.propTypes = {
  date: string.isRequired,
  category: string,
  white: bool
}

export default Meta
