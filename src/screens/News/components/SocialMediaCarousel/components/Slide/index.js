import React from 'react'
import { string } from 'prop-types'
import styled from 'react-emotion'

import Meta from './../../../Meta'

const StyledSlide = styled.a`
  display: flex;
  flex-direction: column;
  margin: 0 5px;
  height: 285px;
  outline: 0;
  color: ${prop => prop.blueBg ? prop.theme.colors.yellowish : prop.theme.colors.darkBlue};
  background: ${prop => prop.blueBg ? prop.theme.colors.blue : prop.theme.colors.wheat};
`

const Cover = styled.div`
  height: 100px;
  background: url(${prop => prop.bgSrc}) center no-repeat;
  background-size: cover; 
`

const Body = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  justify-content: space-between;
  padding: 16px 20px;
`

const Text = styled.p`
  margin: 0;
  max-height: 120px;
  font-size: 14px;
  line-height: 1.2142857143;
  opacity: ${prop => prop.opacity ? 1 : 0.8};
  overflow: hidden;
`

const Slide = ({
  url,
  image_URL,
  content,
  post_time,
  type
}) => (
  <StyledSlide
    blueBg={type === 'twitter'}
    href={url}
    target='_blank'
  >
    {image_URL && <Cover bgSrc={image_URL} />}
    <Body>
      <Text opacity={image_URL}>{content}</Text>
      <Meta
        date={post_time}
        category={type.replace(/(^|\s)\S/, l => l.toUpperCase())}
        white={type === 'twitter'}
      />
    </Body>
  </StyledSlide>
)

Slide.propTypes = {
  url: string.isRequired,
  image_URL: string,
  content: string,
  post_time: string.isRequired,
  type: string.isRequired
}

export default Slide
