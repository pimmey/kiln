import React, { PureComponent } from 'react'
import { array } from 'prop-types'
import { connect } from 'react-redux'
import { Row, Col } from 'react-flexbox-grid'

import ResponsiveGrid from 'Components/ResponsiveGrid'
import Title from 'Components/AllCapsTitle'
import Slide from './components/Slide'
import {
  getSocialFeed,
  selectSocialFeed
} from 'State/ducks/news'

import $ from 'jquery'
import 'slick-carousel'

class SocialMediaCarousel extends PureComponent {
  componentDidMount () {
    // TODO: maybe try and manage this better through store values (in case socialFeed changes while in app)
    if (this.props.socialFeed.length > 0) {
      this.initSlickSlider()
    } else {
      this.props.dispatch(getSocialFeed()).then(() => this.initSlickSlider())
    }
  }

  initSlickSlider = () => $('#social-media-slider').slick({
    dots: false,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 5,
    prevArrow: '<button class="slick-arrow slick-prev"><i class="material-icons">keyboard_arrow_left</i></button>',
    nextArrow: '<button class="slick-arrow slick-next"><i class="material-icons">keyboard_arrow_right</i></button>',
    infinite: true,
    responsive: [{
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    }, {
      breakpoint: 992,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    }]
  })

  componentWillUnmount () {
    $('#social-media-slider').slick('unslick')
  }

  render () {
    const { socialFeed } = this.props

    return (
      <ResponsiveGrid>
        <Row>
          <Col xs={12}>
            <Title>Kiln on social media</Title>
            <div id='social-media-slider' className='jquery-slick'>
              {socialFeed.map((post, index) => (
                <div key={index}>
                  <Slide {...post} />
                </div>
              ))}
            </div>
          </Col>
        </Row>
      </ResponsiveGrid>
    )
  }
}

SocialMediaCarousel.propTypes = {
  socialFeed: array.isRequired
}

export default connect(state => ({
  socialFeed: selectSocialFeed(state)
}))(SocialMediaCarousel)
