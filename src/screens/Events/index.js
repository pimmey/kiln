import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import Media from 'react-media'

import Layout from 'Layouts/DefaultLayout'
import Featured from './components/Featured'
import FeaturedSlider from './components/FeaturedSlider'
import List from './components/List'
import { getEvents } from 'State/ducks/events'

class Events extends PureComponent {
  componentDidMount () {
    this.props.dispatch(getEvents())
  }

  render () {
    return (
      <Layout>
        <Media
          query='(min-width: 768px)'
          render={() => <Featured />}
        />
        <Media
          query='(max-width: 767px)'
          render={() => <FeaturedSlider />}
        />
        <List />
      </Layout>
    )
  }
}

export default connect()(Events)
