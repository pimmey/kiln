import React from 'react'
import styled from 'react-emotion'
import { NavLink } from 'react-router-dom'

import thumbnailer from 'Utils/thumbnailer'
import theme from 'Styles/theme'
import Date from 'Components/EventItem/components/Date'
import Time from 'Components/EventItem/components/Time'
import Cover from 'Components/EventItem/components/Cover'
import Small from 'Components/EventItem/components/SmallText'

const StyledSecondaryEvent = styled(NavLink)`
  display: flex;
  flex-direction: column;
  height: 267px;
  margin-bottom: 20px;
  background: ${prop => prop.theme.colors.blue};
`

const Title = styled.div`
  font-size: 1.25rem;
  line-height: 1.2;
  color: ${prop => prop.theme.colors.yellowish};
`

const FirstRow = styled.div`
  display: flex;
  justify-content: space-between;
  height: 160px;
  color: ${prop => prop.theme.colors.yellowish};
  background: ${prop => prop.theme.colors.darkBlue};
`

const Flex = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 50%;
  padding: 10px 20px 15px;
  height: ${prop => prop.fullHeight && '107px'};
`

const What = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 15px 20px;
  height: 107px;
`

const WhiteOpacity = styled.span`
  color: ${prop => prop.theme.colors.white};
  opacity: 0.5;
`

const secondaryEvent = {
  date: {
    day: 2,
    month: 'February'
  },
  time: '6:00 PM',
  cover: 'https://placekitten.com/420/420',
  title: 'How to become certified Salesforce professional',
  category: 'Lehi'
}

const Secondary = ({
  local_url,
  start_time,
  time,
  image,
  title,
  location,
  category,
  main
}) => (
  <StyledSecondaryEvent to={local_url}>
    <FirstRow>
      <Flex>
        <Date date={start_time} main />
        <Time date={start_time} color={theme.colors.yellowish}/>
      </Flex>
      <Cover
        bgUrl={thumbnailer({ src: image.data.name, dimensions: 600 })}
        className='secondary'
      />
    </FirstRow>
    <What>
      <Title>{title}</Title>
      <Small>
        <WhiteOpacity>
          <Small opacity={0.5}>
            {location && location.data.name}
            &nbsp;&nbsp;&nbsp;&nbsp;
            {category && category.data.name}
          </Small>
        </WhiteOpacity>
      </Small>
    </What>
  </StyledSecondaryEvent>
)

export default Secondary
