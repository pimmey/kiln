import React from 'react'
import { connect } from 'react-redux'
import styled from 'react-emotion'
import { Grid, Row, Col } from 'react-flexbox-grid'

import EventItem from 'Components/EventItem'
import Secondary from './components/Secondary'
import { selectFeaturedEvents } from 'State/ducks/events'
import Animations from 'Styles/animations'

const MainFeaturedPlaceholder = styled.div`
  height: 267px;
  margin-bottom: 20px;
  background: ${prop => prop.theme.colors.green};
  animation: 2s linear infinite ${Animations.pulseAnimation};
`

const SecondaryPlaceholder = styled.div`
  height: 267px;
  margin-bottom: 20px;
  background: ${prop => prop.theme.colors.blue};
  animation: 2s linear infinite ${Animations.pulseAnimation};
`

const Featured = ({ main, secondary }) => (
  <Grid>
    <Row>
      <Col xs={12} md={8}>
        {main.id ? <EventItem main {...main} /> : <MainFeaturedPlaceholder />}
      </Col>
      <Col xs={12} md={4}>
        {secondary.id ? <Secondary {...secondary} /> : <SecondaryPlaceholder />}
      </Col>
    </Row>
  </Grid>
)

export default connect(state => ({
  main: selectFeaturedEvents(state)[0] || {},
  secondary: selectFeaturedEvents(state)[1] || {}
}))(Featured)
