import React from 'react'
import styled from 'react-emotion'
import { NavLink } from 'react-router-dom'

import thumbnailer from 'Utils/thumbnailer'
import theme from 'Styles/theme'
import Small from 'Components/EventItem/components/SmallText'
import When from 'Components/EventItem/components/When'

const StyledSlide = styled(NavLink)`
  display: block;
  color: ${prop => prop.theme.colors.yellowish};
  background: ${prop => prop.theme.colors.blue};
`

const Info = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 160px;
  padding: 12px 20px 18px;
`

const Cover = styled.div`
  width: 50%;
  height: 182px;
  background: url(${prop => prop.bgSrc}) center no-repeat;
  background-size: cover; 
`

const Title = styled.div`
  font-size: 25px;
  line-height: 1.2;
  color: ${prop => prop.theme.colors.yellowish};
`

const Flex = styled.div`
  display: flex;
`

const Slide = ({
  local_url,
  start_time,
  image,
  title,
  category,
  location
}) => (
  <StyledSlide to={local_url}>
    <Flex>
      <When
        date={start_time}
        main
        timeColor={theme.colors.yellowish}
      />
      <Cover bgSrc={thumbnailer({ src: image.data.name, dimensions: 1000 })} />
    </Flex>
    <Info>
      <Title>{title}</Title>
      <Small opacity={0.5}>
        {location && location.data.name}
        &nbsp;&nbsp;&nbsp;&nbsp;
        {category && category.data.name}
      </Small>
    </Info>
  </StyledSlide>
)

export default Slide
