import React from 'react'
import { connect } from 'react-redux'

import MobileSlider from 'Components/MobileSlider'
import Slide from './components/Slide'
import { selectFeaturedEvents } from 'State/ducks/events'

const FeaturedSlider = ({ featuredEvents }) => (
  <MobileSlider marginBottom>
    {featuredEvents.map(item => (
      <div key={item.id}>
        <Slide {...item} />
      </div>
    ))}
  </MobileSlider>
)

export default connect(state => ({
  featuredEvents: selectFeaturedEvents(state).slice(0, 2)
}))(FeaturedSlider)
