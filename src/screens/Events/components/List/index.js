import React, { PureComponent } from 'react'
import { array } from 'prop-types'
import { connect } from 'react-redux'
import styled from 'react-emotion'
import { Row, Col } from 'react-flexbox-grid'

import ResponsiveGrid from 'Components/ResponsiveGrid'
import EventItem from 'Components/EventItem'
import LoadMore from 'Components/LoadMore'
import { selectEvents } from 'State/ducks/events'
import Animations from 'Styles/animations'

const EventItemPlaceholder = styled.div`
  height: 163px;
  margin-bottom: 20px;
  background: ${prop => prop.theme.colors.wheat};
  animation: 2s linear infinite ${Animations.pulseAnimation};
`

class List extends PureComponent {
  state = {
    sliceFrom: 0,
    sliceTo: 0
  }

  handleClick = () => {
    const {
      sliceTo
    } = this.state

    this.setState({
      sliceFrom: 4,
      sliceTo: sliceTo === 0 ? 5 : sliceTo + 4
    })
  }

  render () {
    const {
      sliceFrom,
      sliceTo
    } = this.state

    const { events } = this.props
    const nothingToLoad = events.length <= sliceTo

    if (events.length === 0) {
      return (
        <ResponsiveGrid>
          <Row>
            {Array.from(Array(4)).map((item, index) => (
              <Col xs={6} md={6} key={index}>
                <EventItemPlaceholder />
              </Col>
            ))}
          </Row>
        </ResponsiveGrid>
      )
    }

    return (
      <ResponsiveGrid>
        <Row>
          {events.slice(0, 4).map(event => (
            <Col xs={6} md={6} key={event.id}>
              <EventItem {...event} />
            </Col>
          ))}
        </Row>
        <Row>
          {events.slice(sliceFrom, sliceTo).map(event => (
            <Col xs={6} md={6} key={event.id}>
              <EventItem {...event} />
            </Col>
          ))}
          <Col xs={12}>
            {!nothingToLoad && <LoadMore onClick={this.handleClick} />}
          </Col>
        </Row>
      </ResponsiveGrid>
    )
  }
}

List.propTypes = {
  events: array.isRequired
}

export default connect(state => ({
  events: selectEvents(state)
}))(List)
