import React from 'react'
import {
  BrowserRouter,
  Route
} from 'react-router-dom'

import ScrollToTop from 'Components/ScrollToTop'
import Landing from './Landing'
import News from './News'
import Article from './Article'
import Events from './Events'
import Event from './Event'

const Router = () => (
  <BrowserRouter>
    <ScrollToTop>
      <Route exact path='/' component={Landing} />
      <Route exact path='/news' component={News} />
      <Route path='/news/:slug' component={Article} />
      <Route exact path='/events' component={Events} />
      <Route path='/events/:slug' component={Event} />
    </ScrollToTop>
  </BrowserRouter>
)

export default Router
