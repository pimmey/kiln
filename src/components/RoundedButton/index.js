import React from 'react'
import styled from 'react-emotion'

const Button = styled.a`
  display: flex;
  justify-content: center;
  align-items: center;
  min-width: 120px;
  height: 50px;
  padding: 0 30px;
  font-size: 14px;
  font-weight: bold;
  color: ${prop => prop.theme.colors.white};
  background: ${prop => prop.theme.colors.darkBlue};
  text-transform: uppercase;
  border-radius: 27.5px;
  border: 0;
  outline: 0;
  cursor: pointer;
  -webkit-appearance: none;
  
  @media only screen and (max-width: 767px) {
    color: ${prop => prop.theme.colors.opaqueBlue};
    background: #aeb1ae;
  }
`

const RoundedButton = props => <Button {...props} />

export default RoundedButton
