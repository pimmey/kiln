import styled from 'react-emotion'

export const SpaceBetweenCenter = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`
