import React from 'react'
import {
  any,
  bool
} from 'prop-types'
import styled from 'react-emotion'

const Wrapper = styled.section`
  background: ${prop => prop.white ? '#fff' : 'inherit'};
  padding: 60px 48px 0;
  
  @media screen and (max-width: 767px) {
    padding: 35px 25px 0;
  }
`

const PaddedSection = ({ white, children }) => <Wrapper white={white}>{children}</Wrapper>

PaddedSection.propTypes = {
  white: bool,
  children: any.isRequired
}

export default PaddedSection
