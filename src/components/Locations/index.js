import React, { PureComponent } from 'react'
import styled from 'react-emotion'

import lehiImgSrc from 'Images/building-lehi.png'
import slcImgSrc from 'Images/building-slc.png'
import Text from 'Components/ColoredText'
import Config from 'Utils/config'

const Container = styled.div`
  width: ${prop => prop.theme.sideBarWidth}px;
  padding: 25px 40px;
  margin: -29px 0 35px -40px;
  border-top: 1px solid rgba(83, 132, 153, .2);
  border-bottom: 1px solid rgba(83, 132, 153, .2);
  
  @media only screen and (max-width: 767px) {
    width: calc(100% + 80px);
  }
`

const Img = styled.img`
  display: block;
  width: 190px;
  margin: 10px auto 0;
`

const Tab = styled.div`
  display: ${prop => prop.show ? 'block' : 'none'};
`

const TabSwitcher = styled.a`
  color: ${prop => prop.active ? prop.theme.colors.white : prop.theme.colors.opaqueWhite};
  font-weight: ${prop => prop.active ? 'bold' : 'normal'};
`

const LocationInfo = styled.div`
  display: flex;
  align-items: flex-end;
  justify-content: space-between;
  font-size: 12px;
  line-height: 14px;
  color: ${prop => prop.theme.colors.opaqueWhite}; 
`

class Locations extends PureComponent {
  state = {
    selectedLocation: 'lehi'
  }

  selectLocation = e => {
    e.preventDefault()

    this.setState({
      selectedLocation: e.target.id
    })
  }

  render () {
    const { selectedLocation } = this.state
    const {
      Lehi,
      SLC
    } = Config.locations

    return (
      <Container>
        <strong>
          <Text.coral>Locations:</Text.coral>
        </strong>
        &nbsp;&nbsp;
        <TabSwitcher href='#'
          onClick={this.selectLocation}
          active={selectedLocation === 'lehi'}
          id='lehi'>
          Lehi
        </TabSwitcher>
        &nbsp;&nbsp;
        <TabSwitcher
          href='#'
          onClick={this.selectLocation}
          active={selectedLocation === 'slc'}
          id='slc'>
          SLC
        </TabSwitcher>
        <Tab show={selectedLocation === 'lehi'}>
          <Img src={lehiImgSrc} alt='Lehi' />
          <LocationInfo>
            <span>
              {Lehi.where}
              <br />
              {Lehi.at}
            </span>
            <span>
              —&nbsp;{Lehi.when}
            </span>
          </LocationInfo>
        </Tab>
        <Tab show={selectedLocation === 'slc'}>
          <Img src={slcImgSrc} alt='SLC' />
          <LocationInfo>
            <span>
              {SLC.where}
              <br />
              {SLC.at}
            </span>
            <span>
              —&nbsp;{SLC.when}
            </span>
          </LocationInfo>
        </Tab>
      </Container>
    )
  }
}

export default Locations
