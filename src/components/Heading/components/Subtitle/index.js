import React from 'react'
import {
  string,
  number
} from 'prop-types'
import styled from 'react-emotion'

const StyledSubtitle = styled.p`
  max-width: ${prop => prop.width ? `${prop.width}px` : '415px'};
  margin-bottom: 35px;
  font-size: 1.125rem;
  line-height: 1.1666666667;
`

const Subtitle = ({ children, width }) => <StyledSubtitle width={width}>{children}</StyledSubtitle>

Subtitle.propTypes = {
  children: string.isRequired,
  width: number
}

export default Subtitle
