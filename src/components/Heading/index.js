import React from 'react'
import {
  string,
  number
} from 'prop-types'
import styled from 'react-emotion'

import Title from './components/Title'
import Subtitle from './components/Subtitle'

const StyledHeading = styled.div`
  color: ${prop => prop.theme.colors.blue};
`

const Heading = ({ title, subtitle, subtitleWidth }) => (
  <StyledHeading>
    <Title>{title}</Title>
    <Subtitle width={subtitleWidth}>{subtitle}</Subtitle>
  </StyledHeading>
)

Heading.propTypes = {
  title: string.isRequired,
  subtitle: string.isRequired,
  subtitleWidth: number
}

export default Heading
