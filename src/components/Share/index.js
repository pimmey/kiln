import React, { PureComponent } from 'react'
import styled, { keyframes } from 'react-emotion'

const fadeIn = keyframes`
  0% {
    opacity: 0;
    margin-top: -25px;
    transform: translateZ(0);
  }
  
  90% {
    opacity: 1;
  }
  
  100% {
    top: auto;
    opacity: 1;
    margin-top: 10px;
  }
`

const Wrapper = styled.div`
  margin: 25px 0 0 40px;
  
    &.active .networks a {
    animation: ${fadeIn} .5s ease both;
  }
`

const Button = styled.a`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 45px;
  height: 45px;
  color: ${prop => prop.active ? prop.theme.colors.white : prop.theme.colors.darkBlue};
  background: ${prop => prop.active ? prop.theme.colors.darkBlue : prop.theme.colors.white};
  border-radius: 50%;
  cursor: pointer;
`

const Networks = styled.div`
  margin-left: 2.5px;
`

const SocialNetwork = styled.a`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 40px;
  height: 40px;
  margin-top: 10px;
  font-size: 20px;
  color: ${prop => prop.theme.colors.white};
  border-radius: 50%;
  opacity: 0;
  
  &.facebook {
    background: #3b5998;
    animation-delay: 0.1s;
  }
  
  &.twitter {
    background: #1da1f2;
    animation-delay: 0.2s; 
  }
  
  &.linkedin {
    background: #0077b5;
    animation-delay: 0.3s;
  }
  
  &.google-plus {
    background: #de5e53;
    animation-delay: 0.4s;
  }
`

const hostUrl = `${window.location.protocol}//${window.location.host}`

const networks = [
  {
    name: 'facebook',
    url: ({ url }) =>
      `https://www.facebook.com/sharer/sharer.php?u=${encodeURIComponent(hostUrl + url)}`
  },
  {
    name: 'twitter',
    url: ({ title, url }) =>
      `https://twitter.com/intent/tweet/?text=${encodeURIComponent(title)}&url=${encodeURIComponent(hostUrl + url)}&via=kilnspace&hashtags=kiln,kilnspace`
  },
  {
    name: 'linkedin',
    url: ({ url, title, summary }) =>
      `https://www.linkedin.com/shareArticle?mini=true&url=${encodeURIComponent(hostUrl + url)}&title=${encodeURIComponent(title)}&source=https%3A%2F%2Fkilnspace.com%2F&summary=${encodeURIComponent(summary)}`
  },
  {
    name: 'google-plus',
    url: ({ url }) =>
      `https://plus.google.com/share?url=${encodeURIComponent(hostUrl + url)}`
  }
]

class Share extends PureComponent {
  state = {
    active: false
  }

  handleClick = e => this.setState({ active: !this.state.active })

  render () {
    return (
      <Wrapper className={this.state.active ? 'active' : ''}>
        <Button
          onClick={this.handleClick}
          active={this.state.active}
          className='share-button'
        >
          <i className='material-icons'>reply</i>
        </Button>
        <Networks className='networks'>
          {networks.map(({ name, url }) => (
            <SocialNetwork
              key={name}
              className={name}
              href={url(this.props)}
              target='_blank'
            >
              <i className={`icon icon-${name}`} />
            </SocialNetwork>
          ))}
        </Networks>
      </Wrapper>
    )
  }
}

export default Share
