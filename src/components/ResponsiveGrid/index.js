import React from 'react'
import styled from 'react-emotion'
import { Grid } from 'react-flexbox-grid'

const StyledGrid = styled(Grid)`
  @media only screen and (max-width: 767px) {
    padding-right: 10px;
    padding-left: 10px;
  }
`

const ResponsiveGrid = ({ children }) => <StyledGrid>{children}</StyledGrid>

export default ResponsiveGrid
