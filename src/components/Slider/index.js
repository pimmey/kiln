import React from 'react'
import {
  number,
  object,
  bool
} from 'prop-types'
import styled from 'react-emotion'
import SlickSlider from 'react-slick'
import 'slick-carousel/slick/slick.css'

import ArrowButton from './components/ArrowButton'

const StyledSlider = styled(SlickSlider)`
  min-height: 285px;
  margin-left: -5px;
  margin-right: -5px;
  ${prop => prop.sliderStyles.lg};
  
  @media only screen and (max-width: 767px) {
    ${prop => prop.sliderStyles.sm};
  }
`

const defaultSettings = {
  dots: false,
  speed: 500,
  slidesToShow: 5,
  slidesToScroll: 1,
  prevArrow: <ArrowButton iconLabel='keyboard_arrow_left' />,
  nextArrow: <ArrowButton iconLabel='keyboard_arrow_right' />
}

const Slider = ({
  children,
  slidesToShow,
  rightArrowStyles,
  sliderStyles,
  slidesToShowOnTablet,
  infinite,
  slidesToScrollOnTablet,
  slidesToScrollOnMobile,
  ...props
}) => (
  <StyledSlider
    {...defaultSettings}
    slidesToShow={slidesToShow}
    rightArrowStyles={rightArrowStyles}
    sliderStyles={sliderStyles}
    infinite={infinite}
    responsive={[{
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: slidesToScrollOnMobile || 1
      }
    }, {
      breakpoint: 992,
      settings: {
        slidesToShow: slidesToShowOnTablet,
        slidesToScroll: slidesToScrollOnTablet || 1
      }
    }]}
    {...props}
  >
    {children}
  </StyledSlider>
)

Slider.propTypes = {
  slidesToShow: number.isRequired,
  rightArrowStyles: object,
  sliderStyles: object,
  slidesToShowOnTablet: number,
  infinite: bool
}

Slider.defaultProps = {
  slidesToShowOnTablet: 3,
  sliderStyles: {
    lg: {},
    sm: {}
  },
  infinite: true
}

export default Slider
