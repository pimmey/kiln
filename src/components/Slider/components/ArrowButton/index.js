import React from 'react'
import styled from 'react-emotion'

const StyledArrowButton = styled.button`
  position: absolute;
  top: -52px;
  width: 32px;
  height: 32px;
  color: ${prop => prop.theme.colors.darkBlue};
  background: transparent;
  border: 1px solid rgba(42, 56, 70, .2);
  border-radius: 50%;
  cursor: pointer;
  
  &.slick-prev {
    left: 20px;
  }
  
  &.slick-next {
    right: 20px;
  }
`

const Icon = styled.i`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translateX(-50%) translateY(-50%);
  font-size: 20px;
`

const ArrowButton = ({ ...props }) => (
  <StyledArrowButton {...props}>
    <Icon className='material-icons'>{props.iconLabel}</Icon>
  </StyledArrowButton>
)

export default ArrowButton
