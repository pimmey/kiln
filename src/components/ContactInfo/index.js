import React from 'react'
import styled from 'react-emotion'

const ContactInfoWrapper = styled.div`
  font-size: 14px;
  color: ${prop => prop.theme.colors.opaqueWhite};
  
  @media only screen and (min-width: 768px) and (max-width: 1023px) {
    text-align: right;
  }
`

const Link = styled.a`
  color: ${prop => prop.theme.colors.opaqueWhite};
`

const ContactInfo = () => (
  <ContactInfoWrapper>
    <Link href='tel:385-707-5662'>385-707-5662</Link>
    <br />
    <Link href='mailto:hello@kilnspace.com'>hello@kilnspace.com</Link>
  </ContactInfoWrapper>
)

export default ContactInfo
