import React from 'react'
import styled from 'react-emotion'

const StyledSocialLinksWrapper = styled.div`
  @media only screen and (min-width: 480px) and (max-width: 1023px) {
    margin-top: 15px;
    text-align: right;
  }
`

const SocialLinksWrapper = ({ children }) => <StyledSocialLinksWrapper>{children}</StyledSocialLinksWrapper>

export default SocialLinksWrapper
