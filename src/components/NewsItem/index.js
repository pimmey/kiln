import React from 'react'
import {
  string,
  object,
  any,
  bool
} from 'prop-types'
import styled from 'react-emotion'
import { NavLink } from 'react-router-dom'

import thumbnailer from 'Utils/thumbnailer'
import Meta from 'Screens/News/components/Meta'

const StyledNewsItem = styled(NavLink)`
  display: flex;
  flex-direction: column;
  height: 330px;
  margin-bottom: 20px;
  outline: 0;
  background: ${prop => prop.green ? prop.theme.colors.green : prop.theme.colors.wheat};
  ${prop => prop.styles && prop.styles.lg};
  
  @media only screen and (max-width: 767px) {
    height: 286px;
    margin-bottom: 10px;
    ${prop => prop.styles && prop.styles.sm};
  }
`

const Cover = styled.div`
  height: 155px;
  background: url(${prop => prop.bgSrc}) center no-repeat;
  background-size: cover; 
  
  @media only screen and (max-width: 767px) {
    height: 110px;
  }
`

const Body = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  justify-content: space-between;
  padding: 16px 20px;
`

const Title = styled.div`
  font-size: 1.25rem;
  line-height: 1.2;
  color: ${prop => prop.darkBlue ? prop.theme.colors.darkBlue : prop.theme.colors.yellowish};
`

const Text = styled.p`
  line-height: 1.1875;
  color: ${prop => prop.darkBlue ? prop.theme.colors.darkBlue : 'rgba(42, 56, 70, .5)'};
`

const NewsItem = ({
  styles,
  local_url,
  image,
  title,
  post_time,
  category,
  teaser,
  moreNews
}) => (
  <StyledNewsItem
    green={image || moreNews ? 0 : 1}
    to={local_url}
    styles={styles}
  >
    {image && <Cover bgSrc={thumbnailer({ src: image.data.name, dimensions: 600 })} />}
    <Body>
      <div>
        <Title darkBlue={image || moreNews ? 1 : 0}>{title}</Title>
        {!image && <Text darkBlue={image || moreNews ? 0 : 1}>{teaser}</Text>}
      </div>
      <Meta
        date={post_time}
        category={category && category.data.name}
      />
    </Body>
  </StyledNewsItem>
)

NewsItem.propTypes = {
  styles: object,
  local_url: string.isRequired,
  image: any,
  title: string.isRequired,
  post_time: string.isRequired,
  category: any,
  teaser: string,
  moreNews: bool
}

export default NewsItem
