import React from 'react'
import styled from 'react-emotion'

const StyledButton = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 46px;
  color: ${prop => prop.theme.colors.darkBlue};
  background: ${prop => prop.theme.colors.coral};
  border: none;
  
  &:hover,
  &:focus {
    background: ${prop => prop.theme.colors.coralHover};
  }
`

const Button = props => <StyledButton {...props} />

export default Button
