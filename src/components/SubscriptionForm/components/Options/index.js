import React from 'react'
import { func } from 'prop-types'
import styled from 'react-emotion'

const List = styled.ul`
  margin: -10px 0 20px 0;
  font-size: 14px;
  
  @media only screen and (min-width: 768px) and (max-width: 1023px) { // TODO: get from theme
    position: absolute;
    top: 80px;
    
    & li {
      display: inline-block;
      
      &:first-of-type {
        margin-right: 20px;
      }
    }
  }
`

const ListItem = styled.li`
  margin-bottom: 13px;
`

const InvisibleInput = styled.input`
  display: none;
  
  & + label::before {
    position: relative;
    top: 2px;
    margin-right: 7px;
    font-family: 'Material Icons';
    color: ${prop => prop.theme.colors.coral};
    content: '\\E836';
  }
  
  &:checked + label {
    color: ${prop => prop.theme.colors.white};
  }
  
  &:checked + label::before {
     content: '\\E86C';
  }
`

const Label = styled.label`
  color: rgba(255, 255, 255, .4);
`

// TODO: needs code splitting
const Checkboxes = ({
  onChange: handleChange,
  formLocation,
  id,
  membershipRequestValue,
  techEventsRequestValue
}) => (
  <List>
    <ListItem>
      <InvisibleInput
        type='checkbox'
        value='1'
        name='group[5783][1]'
        id={`mce-group[5783]-5783-0-${formLocation}-${id}`}
        onChange={handleChange}
        checked={membershipRequestValue}
      />
      <Label htmlFor={`mce-group[5783]-5783-0-${formLocation}-${id}`}>
        Interested in membership
      </Label>
    </ListItem>
    <ListItem>
      <InvisibleInput
        type='checkbox'
        value='2'
        name='group[5783][2]'
        id={`mce-group[5783]-5783-1-${formLocation}-${id}`}
        onChange={handleChange}
        checed={techEventsRequestValue}
      />
      <Label htmlFor={`mce-group[5783]-5783-1-${formLocation}-${id}`}>
        Interested in tech & creative events
      </Label>
    </ListItem>
  </List>
)

Checkboxes.propTypes = {
  onChange: func.isRequired
}

export default Checkboxes
