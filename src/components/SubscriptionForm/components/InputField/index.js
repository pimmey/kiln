import React from 'react'
import styled from 'react-emotion'

const StyledInput = styled.input`
  width: 100%;
  padding: 6px 0;
  margin-bottom: 25px;
  color: ${prop => prop.theme.colors.white};
  background: transparent;
  border: 0;
  border-radius: 0;
  border-bottom: 1px solid ${prop => prop.theme.colors.coral};
  -webkit-appearance: none;
  
  &.has-error {
    color: ${prop => prop.theme.colors.error};
    border-bottom-color: ${prop => prop.theme.colors.error};
    
    &::placeholder {
      color: ${prop => prop.theme.colors.error};
    } 
  }
  
  @media only screen and (min-width: 768px) and (max-width: 1023px) { // TODO: get from theme
    margin-right: 13px;
    margin-bottom: 0;
  }
`

const Input = props => <StyledInput {...props} />

export default Input
