import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import styled from 'react-emotion'
import jsonp from 'jsonp'
import qs from 'qs'

import Config from 'Utils/config'
import InputField from './components/InputField'
import Options from './components/Options'
import SubmitButton from 'Components/Button'
import {
  updateField,
  selectValues
} from 'State/ducks/subscriptionForm'

const Form = styled.form`
  margin: 30px 0 60px;
  visibility: ${prop => prop.submitted ? 'hidden' : 'visible'};
  
  @media only screen and (min-width: 768px) and (max-width: 1023px) { // TODO: get from theme
    display: flex;
    position: relative;
    margin-top: 0;
  }
`
const Title = styled.h3`
  margin: 0;
  font-size: 1.25rem;
  color: ${prop => prop.theme.colors.coral};
`

const Paragraph = styled.p`
  color: ${prop => prop.theme.colors.white};
`

class SubscriptionForm extends PureComponent {
  state = {
    message: 'Get priority access to the upcoming Kiln space in Lehi.',
    EMAIL: null,
    FNAME: null,
    COMPANY: null,
    'group[5783][1]': true,
    'group[5783][2]': false,
    success: false,
    emailFieldTouched: false,
    formSubmitTouched: false,
    emailValidationClass: null,
    textFieldValidationClasses: {
      FNAME: null,
      COMPANY: null
    }
  }

  handleChange = e => {
    const {
      target: {
        name,
        type,
        checked,
        value: inputValue
      }
    } = e

    const value = type === 'checkbox' ? checked : inputValue

    this.setState({
      [name]: value
    })

    if (name === 'EMAIL') {
      this.validateEmail(value)
    }

    if (type === 'text') {
      this.validateTextField(name, value)
    }

    this.props.dispatch(updateField({
      name,
      value
    }))
  }

  handleSubmit = e => {
    e.preventDefault()

    this.setState({
      formSubmitTouched: true
    })

    const {
      EMAIL,
      FNAME,
      COMPANY
    } = this.state

    if (!EMAIL) {
      this.validateEmail('')
    }

    this.setState({
      textFieldValidationClasses: {
        FNAME: !FNAME ? 'has-error' : 'valid',
        COMPANY: !COMPANY ? 'has-error' : 'valid'
      }
    })

    let params = qs.stringify({
      EMAIL,
      FNAME,
      COMPANY
    })

    if (this.state['group[5783][1]']) {
      params += `&${qs.stringify({
        'group[5783][1]': this.state['group[5783][1]']
      })}`
    }

    if (this.state['group[5783][2]']) {
      params += `&${qs.stringify({
        'group[5783][2]': this.state['group[5783][2]']
      })}`
    }

    jsonp(`${Config.mailChimpUrl}&${params}`, {
      param: 'c'
    }, (err, data) => {
      if (err) {
        return console.error('err', err)
      }

      const { result } = data

      if (result === 'success') {
        this.setState({ success: true })
        this.showSuccessMessage('Thank you! Kiln team is going to be back to you soon.')
      }
    })
  }

  handleBlur = () => {
    this.setState({ emailFieldTouched: true })
  }

  validateTextField = (name, value) => {
    this.setState({
      textFieldValidationClasses: {
        ...this.state.textFieldValidationClasses,
        [name]: value.length > 0 ? 'valid' : 'has-error'
      }
    })
  }

  validateEmail = value => {
    const re = Config.validEmailPattern

    this.setState({
      emailValidationClass: re.test(value) ? 'valid' : 'has-error'
    })
  }

  showSuccessMessage = message => this.setState({ message })

  render () {
    const {
      emailValidationClass,
      emailFieldTouched,
      formSubmitTouched,
      textFieldValidationClasses
    } = this.state

    // TODO: this should be only id really
    const {
      formLocation,
      id
    } = this.props

    return (
      <div>
        <Title>Request early access</Title>
        <Paragraph>{this.state.message}</Paragraph>
        <Form
          onSubmit={this.handleSubmit}
          submitted={this.state.success}
        >
          <InputField
            name='FNAME'
            placeholder='Full Name'
            onChange={this.handleChange}
            className={(emailFieldTouched || formSubmitTouched) && textFieldValidationClasses.FNAME}
          />
          <InputField
            name='COMPANY'
            placeholder='Company'
            onChange={this.handleChange}
            className={(emailFieldTouched || formSubmitTouched) && textFieldValidationClasses.COMPANY}
          />
          <InputField
            name='EMAIL'
            type='email'
            placeholder='Email'
            onChange={this.handleChange}
            onBlur={this.handleBlur}
            className={(emailFieldTouched || formSubmitTouched) && emailValidationClass}
          />
          <Options
            onChange={this.handleChange}
            formLocation={formLocation}
            id={id}
            membershipRequestValue={this.state['group[5783][1]']}
            techEventsRequestValue={this.state['group[5783][2]']}
          />
          <SubmitButton onClick={this.handleSubmit}>Request</SubmitButton>
        </Form>
      </div>
    )
  }
}

export default connect(state => ({
  values: selectValues(state)
}))(SubscriptionForm)
