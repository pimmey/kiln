import React, { PureComponent } from 'react'
import {
  bool,
  object,
  string
} from 'prop-types'
import styled from 'react-emotion'
import jsonp from 'jsonp'
import qs from 'qs'

import Config from 'Utils/config'
import patternSrc from 'images/pattern.png'

const TitleMessageWrapper = styled.div`
  padding: ${prop => prop.horizontal && '13px 20px'};  
`

const Text = styled.p`
  display: ${prop => (!prop.success && prop.eventsSubscribe) && 'none'};
  color: rgba(42, 56, 70, .5);
`

const Form = styled.form`
  display: ${prop => prop.horizontal && 'flex'};
  padding: ${prop => !prop.horizontal && '26px 28px'};
  height: ${prop => prop.horizontal ? 163 : 330}px;
  background: ${prop => prop.success ? prop.theme.colors.green : prop.theme.colors.coral} url(${patternSrc}) no-repeat;
  background-size: 600px;
  background-position: -10px ${prop => prop.success ? 200 : 275}px;
  ${prop => prop.styles && prop.styles.lg};
  
  &.horizontal {
    background-position: -10px ${prop => prop.success ? 90 : 100}px;
    
    ${TitleMessageWrapper} {
      width: ${prop => prop.success ? '100%' : '50%'};
      flex-shrink: 0;
      background: ${prop => prop.success ? 'transparent' : prop.theme.colors.yellowish};
    }
    
    ${Text} {
      color: ${prop => prop.success && prop.theme.colors.yellowish};
    }
  }
  
  @media only screen and (max-width: 767px) {
    height: ${prop => prop.horizontal ? 163 : 286}px;
    height: ${prop => prop.eventsSubscribe && '252px'};
    padding: ${prop => !prop.horizontal && '16px 20px'};
    background-position: -10px ${prop => prop.success ? 150 : prop.eventsSubscribe ? 195 : 245}px;
    ${prop => prop.styles && prop.styles.sm};
  }
`

const Title = styled.div`
  font-size: 1.5625rem;
  text-transform: uppercase;
  
  @media only screen and (max-width: 991px) {
    font-size: 1.2rem;
  }
  
  @media only screen and (max-width: 767px) {
    margin-bottom: ${prop => prop.eventsSubscribe && '28px'};
    font-size: 20px;
  }
`

const Input = styled.input`
  width: 100%;
  padding: 7px 0;
  background: transparent;
  border: none;
  border-bottom: 1px solid ${prop => prop.theme.colors.white};
  border-radius: 0;
  outline: none; 
  
  &::placeholder {
    color: ${prop => prop.theme.colors.darkBlue};
  }
  
  &.has-error {
    color: ${prop => prop.theme.colors.error};
    border-bottom-color: ${prop => prop.theme.colors.error};
    
    &::placeholder {
      color: ${prop => prop.theme.colors.error};
    }
  }
`

const Button = styled.button`
  padding: 10px 0;
  background: transparent;
  border: none;
  -webkit-appearance: none;
  font-size: 12px;
  font-weight: bold;
  text-transform: uppercase;
  color: ${prop => prop.theme.colors.white};
  cursor: pointer;
`

const Inputs = styled.div`
  display: ${prop => prop.hide ? 'none' : 'block'};
  padding: ${prop => prop.horizontal && '17px 22px'};
`

class NewsletterForm extends PureComponent {
  state = {
    EMAIL: null,
    success: false,
    emailValidationClass: null,
    formSubmitTouched: false,
    emailFieldTouched: false,
    title: this.props.title,
    message: this.props.message
  }

  handleSubmit = e => {
    e.preventDefault()

    this.setState({
      formSubmitTouched: true
    })

    const {
      EMAIL,
      emailValidationClass
    } = this.state

    if (!EMAIL) {
      this.validateEmail('')
    }

    const params = qs.stringify({ EMAIL })

    if (emailValidationClass === 'valid') {
      jsonp(`${this.props.mailChimpUrl}&${params}`, {
        param: 'c'
      }, (err, data) => {
        if (err) {
          return console.error('err', err)
        }

        const { result } = data

        if (result === 'success') {
          this.setState({
            success: true,
            title: 'One last step',
            message: 'Please check your email to activate the subscription.'
          })
        }
      })
    }
  }

  handleChange = e => {
    const {
      target: {
        value
      }
    } = e

    this.setState({ EMAIL: value })
    this.validateEmail(value)
  }

  handleBlur = () => {
    this.setState({ emailFieldTouched: true })
  }

  validateEmail = value => {
    this.setState({
      emailValidationClass: Config.validEmailPattern.test(value) ? 'valid' : 'has-error'
    })

    return false
  }

  render () {
    const {
      title,
      message,
      success,
      emailFieldTouched,
      formSubmitTouched,
      emailValidationClass
    } = this.state

    const {
      horizontal,
      styles,
      eventsSubscribe
    } = this.props

    return (
      <Form
        success={success}
        onSubmit={this.handleSubmit}
        horizontal={horizontal}
        className={horizontal ? 'horizontal' : ''}
        styles={styles}
        eventsSubscribe={eventsSubscribe}
      >
        <TitleMessageWrapper horizontal={horizontal}>
          <Title
            eventsSubscribe={eventsSubscribe}
            dangerouslySetInnerHTML={{ __html: title }}
          />
          <Text
            success={success}
            eventsSubscribe={eventsSubscribe}
          >{message}</Text>
        </TitleMessageWrapper>
        <Inputs
          hide={success}
          horizontal={horizontal}
        >
          <Input
            type='email'
            name='EMAIL'
            placeholder='Enter email'
            onChange={this.handleChange}
            onBlur={this.handleBlur}
            className={(emailFieldTouched || formSubmitTouched) && emailValidationClass}
          />
          <Button onClick={this.handleSubmit}>Sign up</Button>
        </Inputs>
      </Form>
    )
  }
}

NewsletterForm.propTypes = {
  horizontal: bool,
  styles: object,
  eventsSubscribe: bool,
  title: string.isRequired,
  message: string.isRequired
}

export default NewsletterForm
