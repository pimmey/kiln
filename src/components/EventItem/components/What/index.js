import React from 'react'
import styled from 'react-emotion'

import Small from 'Components/EventItem/components/SmallText'
import flexSpaceBetween from './../../styles/FlexSpaceBetween'

const StyledWhat = styled.div`
  ${flexSpaceBetween};
  padding: ${prop => prop.main ? '21px 27px 15px' : '15px 20px'};
  background: ${prop => prop.main ? prop.theme.colors.green : prop.theme.colors.wheat};
  
  @media only screen and (max-width: 767px) {
    height: 166px;
  }
`

const Title = styled.div`
  font-size: ${prop => prop.main ? 1.875 : 1.25}rem;
  line-height: 1.2;
  color: ${prop => prop.main ? prop.theme.colors.yellowish : prop.theme.colors.darkBlue};
  
  @media only screen and (max-width: 991px) {
    font-size: ${prop => prop.main && '1.5rem'};
  }
`

const What = ({ main, title, category, location }) => (
  <StyledWhat main={main}>
    <Title main={main}>{title}</Title>
    <Small opacity={0.5}>
      {location}
      &nbsp;&nbsp;&nbsp;&nbsp;
      {category}
    </Small>
  </StyledWhat>
)

export default What
