import React from 'react'
import styled from 'react-emotion'
import moment from 'moment'

import Config from 'Utils/config'

const Date = styled.div`
  color: ${prop => prop.main ? prop.theme.colors.yellowish : prop.theme.colors.darkBlue};
`

const Day = styled.div`
  font-size: 3.125rem;
  margin-bottom: 5px;
  
  @media only screen and (max-width: 767px) {
    font-size: ${prop => prop.main || prop.big ? 50 : 30}px;
    margin-bottom: ${prop => (!prop.main && !prop.big) && '16px'};
  }
`

const Month = styled.div`
  font-size: .75rem;
  font-weight: bold;
  text-transform: uppercase;
  
  @media only screen and (max-width: 767px) {
    font-size: ${prop => prop.main || prop.big ? 12 : 10}px;
  }
`

const EventDateTime = ({ main, date, big }) => (
  <Date main={main}>
    <Day main={main} big={big}>{moment(date, Config.directusDateFormat).format('D')}</Day>
    <Month main={main} big={big}>{moment(date, Config.directusDateFormat).format('MMMM')}</Month>
  </Date>
)

export default EventDateTime
