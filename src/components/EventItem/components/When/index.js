import React from 'react'
import styled from 'react-emotion'

import flexSpaceBetween from './../../styles/FlexSpaceBetween'
import Date from './../Date'
import Time from './../Time'

const StyledWhen = styled.div`
  ${flexSpaceBetween};
  flex-shrink: 0;
  width: 110px;
  padding: 10px 20px 15px;
  
  @media only screen and (max-width: 991px) {
    width: 100px;
  }
  
  @media only screen and (max-width: 767px) {
    display: ${prop => !prop.main && 'block'};
    width: 50%;
    height: ${prop => !prop.main && '86px'};
    background: ${prop => prop.main && prop.theme.colors.darkBlue};
    padding: ${prop => !prop.main && '5px 9px 8px'};
  }
`

const When = ({ main, date, timeColor }) => (
  <StyledWhen main={main}>
    <Date main={main} date={date} />
    <Time main={main} date={date} color={timeColor} />
  </StyledWhen>
)

export default When
