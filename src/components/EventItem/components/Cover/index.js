import React from 'react'
import styled from 'react-emotion'

const StyledCover = styled.div`
  flex-shrink: 0; 
  width: ${prop => prop.main ? 267 : 163}px;
  background: url(${prop => prop.bgUrl}) no-repeat center;
  background-size: cover;
  
  @media only screen and (max-width: 991px) {
    width: ${prop => prop.main ? 196 : 100}px;
    
    &.secondary {
      width: 50%;
    }
  } 
  
  @media only screen and (max-width: 767px) {
    width: 50%;
    height: 86px;
  }
`

const Cover = ({ bgUrl, main, className }) => <StyledCover bgUrl={bgUrl} main={main} className={className} />

export default Cover
