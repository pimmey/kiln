import React from 'react'
import { number } from 'prop-types'
import styled from 'react-emotion'

const Small = styled.div`
  font-size: .75rem;
  color: ${prop => prop.theme.colors.darkBlue};
  opacity: ${prop => prop.opacity};
  
  @media only screen and (max-width: 767px) {
    font-size: 12px;
  }
`

const Text = ({ children, opacity }) => <Small opacity={opacity}>{children}</Small>

Text.propTypes = {
  opacity: number
}

export default Text
