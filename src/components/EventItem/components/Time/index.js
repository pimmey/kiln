import React from 'react'
import { string } from 'prop-types'
import moment from 'moment'
import styled from 'react-emotion'

import theme from 'Styles/theme'
import Config from 'Utils/config'

const Small = styled.div`
  font-size: .75rem;
  color: ${prop => prop.color};
  opacity: ${prop => prop.opacity};
  
  @media only screen and (max-width: 767px) {
    margin-top: ${prop => !prop.main && '1px'};
    font-size: ${prop => prop.main || prop.big ? 12 : 10}px;
    color: ${prop => (!prop.main && !prop.big) && prop.theme.colors.opaqueBlue};
  }
`

const Time = ({ date, color, main, big }) => (
  <Small
    color={color}
    main={main}
    big={big}
  >
    {moment(date, Config.directusDateFormat).format('hh:mm A')}
  </Small>
)

Time.propTypes = {
  date: string.isRequired,
  color: string
}

Time.defaultProps = {
  color: theme.colors.darkBlue
}

export default Time
