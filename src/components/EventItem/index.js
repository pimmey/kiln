import React from 'react'
import styled from 'react-emotion'
import { NavLink } from 'react-router-dom'

import thumbnailer from 'Utils/thumbnailer'
import When from './components/When'
import Cover from './components/Cover'
import What from './components/What'

const StyledEventItem = styled(NavLink)`
  display: flex;
  height: ${prop => prop.main ? 267 : 163}px;
  margin-bottom: 20px;
  background: ${prop => prop.main ? prop.theme.colors.green : prop.theme.colors.yellowish};
  ${prop => prop.styles && prop.styles.lg};
  
  @media only screen and (max-width: 767px) {
    flex-wrap: wrap;
    height: 252px;
    margin-bottom: 10px;
    ${prop => prop.styles && prop.styles.sm};
  }
`

const EventItem = ({
  local_url,
  start_time,
  image,
  title,
  category,
  location,
  main,
  styles
}) => (
  <StyledEventItem
    main={main ? 1 : 0}
    to={local_url}
    styles={styles}
  >
    <When
      main={main}
      date={start_time}
    />
    <Cover
      bgUrl={thumbnailer({ src: image && image.data.name, dimensions: 750 })}
      main={main}
    />
    <What
      main={main}
      title={title}
      category={category && category.data.name}
      location={location && location.data.name}
    />
  </StyledEventItem>
)

export default EventItem
