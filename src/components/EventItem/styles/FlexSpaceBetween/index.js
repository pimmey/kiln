import { css } from 'react-emotion'

const flexSpaceBetween = css`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`

export default flexSpaceBetween
