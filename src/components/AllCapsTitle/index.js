import React from 'react'
import styled from 'react-emotion'

const StyledTitle = styled.h2`
  margin: 24px 0 26px;
  font-size: 1.25rem;
  font-weight: normal;
  text-align: center;
  text-transform: uppercase;
  
  @media only screen and (max-width: 767px) {
    font-size: 16px;
  }
`

const Title = ({ children }) => <StyledTitle>{children}</StyledTitle>

export default Title
