import React from 'react'
import styled from 'react-emotion'
import {
  string,
  number
} from 'prop-types'

import Title from './components/Title'
import Text from './components/Text'
import Price from './components/Price'

const Block = styled.div`
  margin-bottom: 60px;
  
  @media only screen and (max-width: 767px) {
    margin-bottom: 35px;
  }
`

const FeatureBlock = ({ title, text, price, variable }) => (
  <Block>
    <Title>{title}</Title>
    {typeof price !== 'undefined' && <Price price={price} variable={variable} mobile />}
    <Text>{text}</Text>
    {typeof price !== 'undefined' && <Price price={price} variable={variable} />}
  </Block>
)

FeatureBlock.propTypes = {
  title: string.isRequired,
  text: string.isRequired,
  price: number
}

export default FeatureBlock
