import React from 'react'
import { string } from 'prop-types'
import styled from 'react-emotion'

const StyledTitle = styled.h4`
  margin: 0 0 15px;
  font-size: 1.125rem;
  font-weight: bold;
  text-transform: uppercase;
  letter-spacing: 1.8px;
`

const Title = ({ children }) => <StyledTitle>{children}</StyledTitle>

Title.propTypes = {
  children: string.isRequired
}

export default Title
