import React from 'react'
import { string } from 'prop-types'
import styled from 'react-emotion'

const StyledText = styled.p`
  padding-right: 10%;
  margin-bottom: 0;
  line-height: 1.1875;
`

const Text = ({ children }) => <StyledText>{children}</StyledText>

Text.propTypes = {
  children: string.isRequired
}

export default Text
