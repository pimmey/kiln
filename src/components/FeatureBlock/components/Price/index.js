import React from 'react'
import {
  number,
  bool
} from 'prop-types'
import styled from 'react-emotion'

const PriceTag = styled.span`
  font-size: .75rem;
  font-weight: bold;
  letter-spacing: 1.2px;
  text-transform: uppercase;
  
  &::before {
    display: block;
    margin: 15px 0;
    width: 20px;
    height: 1px;
    background: ${prop => prop.theme.colors.coral};
    content: '';
  }
  
  &.mobile {
    display: none;
    margin-top: -12px;
    color: ${prop => prop.theme.colors.coral};
    
    &::before {
      display: none;
    }
  }
  
  @media only screen and (max-width: 767px) {
    &.mobile {
      display: block;
    }
    
    &.non-mobile {
      display: none;
    }
  }
`

const Price = ({ price, variable, mobile }) => (
  price > 0
    ? <PriceTag className={mobile ? 'mobile' : 'non-mobile'}>${price}{variable ? '*' : ''}/mo</PriceTag>
    : <PriceTag className={mobile ? 'mobile' : 'non-mobile'}>Free</PriceTag>
)

Price.propTypes = {
  price: number,
  variable: bool,
  mobile: bool
}

export default Price
