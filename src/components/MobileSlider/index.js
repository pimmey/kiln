import React from 'react'
import styled from 'react-emotion'
import SlickSlider from 'react-slick'
import 'slick-carousel/slick/slick.css'

const StyledSlider = styled(SlickSlider)`
  margin-bottom: ${prop => prop.marginBottom && '10px'};
  
  & .slick-dots {
    position: absolute;
    z-index: 100;
    right: 20px;
    bottom: 8px;
    
    & li {
      display: inline-block;
      width: 15px;
      &.slick-active button::after {
        background: ${prop => prop.theme.colors.darkBlue};
      }
    }
    
    & button {
      width: 10px;
      height: 10px;
      color: transparent;
      background: none;
      border: 0;
      cursor: pointer;
      display: inline-block;
      
      &::after {
        position: absolute;
        top: 0;
        display: block;
        width: 6px;
        height: 6px;
        border-radius: 50%;
        background: rgba(0, 0, 0, .2);
        content: '';
      }
    } 
  }
`

const defaultSettings = {
  dots: true,
  arrows: false,
  infinite: false,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1
}

const Slider = ({ marginBottom, children }) => <StyledSlider marginBottom {...defaultSettings}>{children}</StyledSlider>

export default Slider
