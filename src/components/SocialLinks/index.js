import React from 'react'
import { bool } from 'prop-types'
import styled from 'react-emotion'

const SocialLink = styled.a`
  &:not(:last-of-type) i {
    margin-right: 20px;
  }
`

const SocialIcon = styled.i`
  font-size: 1.5rem;
  color: ${prop => prop.opaqueWhite ? prop.theme.colors.opaqueWhite : prop.theme.colors.coral};
`

const socialNetworks = [
  {
    iconClass: 'icon-twitter',
    link: 'https://twitter.com/kilnspace'
  },
  {
    iconClass: 'icon-facebook',
    link: 'https://www.facebook.com/kilnspace/'
  }
]

const SocialLinks = ({ opaqueWhite }) => socialNetworks.map(({ iconClass, link }) => (
  <SocialLink
    key={iconClass}
    href={link}
    target='_blank'
  >
    <SocialIcon
      className={`icon ${iconClass}`}
      opaqueWhite={opaqueWhite}
    />
  </SocialLink>
))

SocialLinks.propTypes = {
  opaqueWhite: bool
}

export default SocialLinks
