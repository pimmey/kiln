import React from 'react'
import styled from 'react-emotion'

const Button = styled.button`
  display: block;
  margin: 4px auto 26px;
  font-size: 1.25rem;
  text-transform: uppercase;
  color: rgba(42, 56, 70, .4);
  background: transparent;
  border: 0;
  outline: 0;
  -webkit-appearance: none;
  cursor: pointer;
  
  @media only screen and (max-width: 767px) {
    margin-top: 14px;
  }
`

const LoadMore = ({ ...props }) => <Button {...props}>Load more…</Button>

export default LoadMore
