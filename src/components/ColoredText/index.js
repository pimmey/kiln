import React from 'react'
import { css } from 'emotion'
import theme from 'Styles/theme'

const Text = Object.entries(theme.colors).reduce((acc, item) => {
  acc[item[0]] = ({ children }) => <span className={css(`color: ${item[1]}`)}>{children}</span>
  return acc
}, {})

export default Text
